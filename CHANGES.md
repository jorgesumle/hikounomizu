## Hikou no mizu changelog

### 1.0.1
- Added internationalization support
- Added Spanish, French and Norwegian translations
- Added support for detecting user language
- Make the user interface stretch to the translated text size
- Relicensed original data from FreeArt license to CC BY-SA 4.0

### 1.0

- **Networked game: introduced networked multiplayer gaming**
  - Introduced an `enet`-based protocol for networked games
  - Introduced a game server `hikounomizud`
    - Awaits for players to connect, orchestrates a networked game
    - Awaits in lobby mode (players can join and are respawned when KO) until enough players are connected
    - Starts a formal game when enough players are connected
    - When a game is completed, performs matchmaking and optionally change arena
    - Added server documentation in `README`
    - Added an inactivity tracking mechanism to optionally kick inactive players
    - Added a server event mechanism to send informative messages to clients
    - Added version checking (server and client are considered compatible iff only the patch version is different)
    - Added data path checking (shows a warning if the game data was not found by the server)
  - Introduced a game client
    - Connects to a remote game server
    - Displays the current scene and sends inputs
    - Added a connected players/ping dialog visible when pressing tab
    - Added a server event logger that displays incoming server messages
    - Added floating name tags on top of connected players
    - Added a networked game pause tab
  - Introduced a 'Multiplayer' lobby scene
    - Allows selecting a client username and played character
    - Allows connecting to a remote server
    - Added a text input widget to type free utf8 text
      - Supports composition and input method editors (IME)

- **Gameplay**
  - Added a new character: `Hana`! Mid-range gameplay and uses the fire element
  - Added a new arena: `Traezh`, which makes use of the under-used platform system, allows rock climbing
  - Reworked `Evig Sol`, added more climbeable and hitable platforms to add gameplay variety
  - Added a round handling system
    - A game consists of one of (optionally) several rounds
    - The winner is the first one to win the required amount of rounds
    - Up to 5 rounds to win in local games, more in networked games
    - Added a round announcer at the beginning of a round
    - Added a graphical indicator of the number of rounds won per player
    - Added a round completed scene at the end of a fight, with character icon, name and round win statistics
  - Allow simultaneous hits within a short time window and therefore allow draw games
  - Make fight scenes more flexible, allow adding and removing players mid-game (used in networked mode)
  - Defined spawns for at least 6 players in each arena
  - Tweaked characters speed, jump energy, attack timings and strength to increase balance
  - Fixed various hit boxes location and size
  - Disabled key repeat (holding in 'punch' will only punch once)
  - Fixed integration of physics velocity and position
    - Earlier code would produce different jump trajectories for different frame rates
    - Use analytical integration to produce the same trajectories for any time step

- **Graphics**
  - Fixed text size computation in some corner cases
  - Fixed text kerning that had been broken
  - Added a camera shake effect on player hit
  - Added support for 'borderless' or 'native' fullscreen modes
  - Minor graphical glitch fixes on Takino default punch and kick, as well as moving default and kick animations

- **Audio**
  - Added ambient sound support per arena
  - Harmonized and increased sound effects volume
  - Added sound effects for Takino and Hana
  - Added sand step sound effect
  - Added shuriken flying 'whoosh' sound effect
  - Added shuriken to shuriken impact sound effect
  - Added 'Fight!' sound effect at the beginning of a round
  - Added elemental sounds effects (fire, water, lightning) played on some attacks
  - Added ability to configure the pitch variation for each sound effect
    - Reduce the pitch variation for character kick/punch/hit sound effects
  - Harmonized and reduced music volume
    - Added new musics and changed some existing musics (see `datasrc/COPYING`)
  - Improved music error handling (remove a music from the playlist if not found)

- **Joystick**
  - Added SDL game controller support to automatically assign default bindings
  - Added an option to revert the bindings of a game controller to the default

- **GUI**
  - Make display coordinates relative, stretching the GUI to look nice on high and low resolutions
  - Updated the GUI theme to be more consistent and arguably look nicer
  - Unified theme for section headers
  - Re-drawn 'Hikou no mizu' title banner
  - Replaced the default 'Liberation Sans' font by 'Murecho'
  - Replaced the display 'Intuitive' font by 'Mochiy Pop'
  - Rewritten/simplified main menu to allow selecting player count and rounds to win in local games
  - Added an option to select up to 6 players (vs. 5 before the update) for local games
  - Added a dialog widget to notify of an ongoing action (used when connecting to a server)
  - Improved chooser layouts to make all selections reachable
  - Ceil the player health points value to display `1` rather than `0` for a fraction of a health point

- **Configuration**
  - Follow the XDG base directory specification on GNU/Linux
  - Make the configuration file reader robust to empty lines
  - Added a server configuration reader/writer
    - Default configuration can be written to a file using `hikounomizud -e path.cfg`
    - List of configuration options in `README`
  - Added client configuration options
    - Ability to show/hide the frame rate counter
    - Ability to save/read the client username and played character in networked games
    - Ability to enable/disable the new camera shake effect on player hit
    - Ability to change the relative volume of the new ambient sound effects

- **Build system**
  - Allow configuring the amount of parallel tasks for data building (`HNM_PARALLEL` environment variable)
  - Take rounding errors into account in animation rendering to avoid glitches in game
  - Exit data builder cleanly when terminated
  - Add options to build only the server or only the client
  - Added a script `filter_data.sh` to filter out the data that is not needed by the server
  - Working MinGW building

- **Under the hood**
  - Restructured code to separate core gameplay mechanisms from graphics and audio systems
    - Allows the server to run fights in memory
  - Added serializers for primitive types and game objects to be exchanged in networked mode
  - Restart local games much faster by not reloading every piece of data
  - Switch to std::chrono, increasing time measurement precision
  - Moved versus mode handling from `Player` to a dedicated fight mode system
  - Rewritten and much simplified playlist and music playing system
  - Added SDL_Image initialization checks and error handling
  - Make the utf8 decoder more strict to malformed utf8 input
  - Various cleanup and source code modernizing (C++11 standard)
  - Faster compilation by using forward declarations when relevant
  - Dropped GLU dependency

- **Bug fixes**
  - Fixed AI cheating in versus mode (able to hit backwards)
  - Fixed the ability of AI to run into their own shurikens :)
  - Fixed the ability to jump higher by calling `jump()` multiple times in a row
  - Added a physics epsilon to avoid collision errors due to float rounding errors
    - Especially relevant in networked mode which transmits floats at a lower precision
  - Fixed a bug that could allow selecting only one player for a local game
  - Fixed a bug that would have done weird things when loading a malformed ogg file
  - Generally improved error checking and robustness of resources loading
  - Use `ov_fopen` to open sound files rather than `ov_open` that causes permission issues on some platforms

- **Documentation**
  - Improved logs and added library information logs
  - Made the code documentation style consistent
  - Rewrote `README` to use markdown
  - Moved the changelog to a dedicated file `CHANGES.md`
  - Moved the data license to `datasrc/COPYING`


### 0.9.2

- Ported the xml code from (unmaintained) `TinyXML1` to `pugixml`
- More robust xml parsing
- Dropped `fmt` dependency

### 0.9.1

- Fixed text rendering glitches which occured on some platforms
- Added an option to enable vertical synchronization, or cap frame rate
- Significantly improved loading times by compressing spritesheets with pngquant
- Added support for color and emphasis on the command line output
- Fixed an issue in the saving and loading of joystick key bindings
- Switched to C++11, modernizing source code
  - Replaced `pthread` with the standard `std::thread` library
  - Fixed cast and float comparison warnings

### 0.9

- **Audio & Sound**
  - Added a flexible sound effect system
    - Sound effects are described for each animation
    - Variation of pitch to increase realism
  - Footstep sounds depend on the ground material
  - Added weapon hit, ground hit, and air friction sound effects
  - Added several musics for fight scenes
  - Added UI sounds for character & arena selection

- **Graphics**
  - Automated scaling of the framesets generated from source animations
  - Animations regenerated, scaled up for a 1080p gameplay
  - Smooth movement of the dynamic fight camera
  - Takino: Redrawn default punch animation
  - More flexible display mode option, especially windowed
  - UI elements scale to match the display mode

- **Gameplay**
  - Added a jump state, with its own attacks and hit animations
  - Added a versus mode, two players always facing each other
  - Added a specific camera zoom level per arena
  - Added a remaining throwable weapon interface counter
  - Physics of throwable weapons improved
  - Starting position of players improved

- **UI & Options**
  - Added sound volume control sliders
  - Moved key binders to a new tab
  - Removed 'apply' button in the options
  - Pressing escape resets the main menu
  - Text height computation fixed
