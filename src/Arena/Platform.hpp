/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLATFORM
#define DEF_PLATFORM

#include "Engines/Physics/PhysicsObject.hpp"

class SoundInterface; ///< For playing platform impact sound effects

/// A simple physics platform: May be stood on and may be hitable
class Platform : public PhysicsObject
{
    public:
        Platform();

        /// Position and size of the pixels in the texture file
        void setSourceBox(const Box &sourceBox);
        const Box &getSourceBox() const;

        /// Size of the platform when drawn
        void setDrawSize(const Vector &drawSize);
        const Vector &getDrawSize() const;

        /// Body box relatively to (0,0,drawSize)
        void setBodyBox(const Box &spawnBox);
        const Box &getBodyBox() const;

        /// Spawn position of the platform body
        void setSpawnPosition(const Vector &spawnPosition);
        const Vector &getSpawnPosition() const;

        void setHitable(bool hitable);
        void setSoundInterface(SoundInterface *soundInterface);

        void collideWorld(bool /*ground*/) override;
        void collide(PhysicsObject& /*obj*/) override;
        void takeAHit(float strength, HitDirection side,
                      const Player */*source*/) override;
        void pushDamage(float strength, HitDirection side,
                        const Player &source,
                        DamageTracker &/*damage*/) override;

        bool interceptsShadows() const override;

    private:
        SoundInterface *m_soundInterface;
        Box m_sourceBox, m_bodyBox;
        Vector m_drawSize, m_spawnPosition;
        bool m_hitable;
};

#endif
