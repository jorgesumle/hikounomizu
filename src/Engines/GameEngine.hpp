/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_GAME_ENGINE
#define DEF_GAME_ENGINE

#include "Screens/MainMenu/MainMenu.hpp"
#include "Screens/Options/Options.hpp"
#include "Screens/CharactersMenu/CharactersMenu.hpp"
#include "Screens/ArenaMenu/ArenaMenu.hpp"
#include "Screens/LocalGame/LocalGame.hpp"
#include "Screens/NetworkedGame/NetworkedGame.hpp"

#include <cstddef>
#include <vector>
#include <string>

class Window;
class Configuration;
class JoystickManager;

class FightRules; ///< To configure a local fight rules
struct Character; ///< To configure a local fight characters

/// Handling of game sections (menu, character selection, fight...)
/// It manages and ordains the screens while providing key resources
class GameEngine
{
    public:
        GameEngine(Window &window,
                   Configuration &configuration,
                   JoystickManager &joyManager);
        GameEngine(const GameEngine &copied) = delete;
        GameEngine &operator=(const GameEngine &copied) = delete;

        //Main menu
        void initMainMenu();

        //Options
        void initOptions();

        //Characters menu
        void confCharactersMenu_playersNo(std::size_t playersNo);
        void initCharactersMenu();

        //Arena menu
        void initArenaMenu();

        //Fight screen
        void confLocalGame_arena(const std::string &arenaName);
        void confLocalGame_players(const std::vector<Character> &players);
        void confLocalGame_rules(const FightRules &rules);
        void initLocalGame();

        //Networked game
        void initNetworkedGame();

        void launchNextScreen();
        void exit();
        bool wasExited() const;

        Window &getWindow() const;
        Configuration &getConfiguration() const;
        JoystickManager &getJoystickManager() const;

    private:
        void setNextScreen(Screen &nextScreen);

        Window *m_window;
        Configuration *m_configuration;
        JoystickManager *m_joyManager;

        Screen *m_nextScreen;

        MainMenu m_mainMenu;
        Options m_options;
        CharactersMenu m_charactersMenu;
        ArenaMenu m_arenaMenu;
        LocalGame m_localGame;
        NetworkedGame m_networkedGame;

        bool m_exited;
};

#endif
