/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SOUND_BUFFER_MANAGER
#define DEF_SOUND_BUFFER_MANAGER

#include "Audio/SoundBuffer.hpp"

#include <map>
#include <string>

/// Class loading and keeping track of requested sound buffers
/// to load them only once
class SoundBufferManager
{
    public:
        SoundBufferManager() = default;
        SoundBufferManager(const SoundBufferManager &copied) = delete;
        SoundBufferManager &operator=(const SoundBufferManager &copied) = delete;
        ~SoundBufferManager();

        bool getSoundBuffer(const std::string &soundPath, SoundBuffer* &target);

    private:
        SoundBuffer *findLoadedSoundBuffer(const std::string &soundPath);
        bool loadSoundBuffer(const std::string &soundPath, SoundBuffer* &target);

        std::map<std::string, SoundBuffer*> m_soundBuffersList;
};

#endif
