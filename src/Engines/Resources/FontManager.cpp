/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FontManager.hpp"

#include "Tools/Log.hpp"
#include "Tools/Format.hpp"

namespace
{
    /// Flag to only print the Freetype library version once
    bool loggedFreetypeVersion = false;
}

FontManager::FontManager()
{
    FT_Init_FreeType(&m_library);

    if (!loggedFreetypeVersion)
    {
        FT_Int vmajor, vminor, vpatch;
        FT_Library_Version(m_library, &vmajor, &vminor, &vpatch);
        Log::info(Format::format("FreeType {}.{}.{}",
                                 vmajor, vminor, vpatch));
        loggedFreetypeVersion = true;
    }
}

FontManager::~FontManager()
{
    std::map<std::pair<std::string, int>, Font*>::iterator it;
    for (it = m_fontsList.begin(); it != m_fontsList.end(); ++it)
        delete it->second;

    FT_Done_FreeType(m_library);
}

Font &FontManager::getFont(const std::string &path, int fontSize)
{
    Font *foundFont = findLoadedFont(path, fontSize);

    if (foundFont == nullptr)
        return loadFont(path, fontSize);

    return (*foundFont);
}

Font &FontManager::getDisplay(int fontSize)
{
    return getFont("fonts/mochiypop.ttf", fontSize);
}

Font &FontManager::getDefault(int fontSize)
{
    return getFont("fonts/murecho.otf", fontSize);
}

Font *FontManager::findLoadedFont(const std::string &path, int fontSize)
{
    std::map<std::pair<std::string, int>, Font*>::iterator it;
    it = m_fontsList.find( std::make_pair(path, fontSize) );

    if (it != m_fontsList.end())
        return it->second;

    return nullptr;
}

Font &FontManager::loadFont(const std::string &path, int fontSize)
{
    Font *loadedFont = new Font(m_library, path, fontSize);
    m_fontsList[ std::make_pair(path, fontSize) ] = loadedFont;

    return (*loadedFont);
}
