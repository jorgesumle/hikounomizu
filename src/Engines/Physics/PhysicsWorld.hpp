/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PHYSICS_WORLD
#define DEF_PHYSICS_WORLD

#define MATERIAL_NONE "none"

#include "Structs/Box.hpp"
#include <vector>
#include <string>

class PhysicsObject;

class PhysicsWorld
{
    public:
        PhysicsWorld();
        PhysicsWorld(const Box &worldBox, float gravity, float airFriction);

        void update(float frameTime);

        void addObject(PhysicsObject &object);
        void removeObject(const PhysicsObject &object);
        const std::vector<PhysicsObject*> &getObjects() const;

        void pulseObject(PhysicsObject &object, float xPulse, float yPulse);
        void resetVelocities();

        std::vector<PhysicsObject*> hitTest(const PhysicsObject &object) const;
        std::vector<PhysicsObject*> hitTest(const PhysicsObject &object,
                                            const Box &customBox) const;
        bool worldContains(const Box &box) const;
        bool isFree(const Box &box) const;

        void setWorldBox(const Box &worldBox);
        const Box &getWorldBox() const;

        void setGroundMaterial(const std::string &material);
        const std::string &getGroundMaterial() const;

        void setGravityForce(float gravity);
        void setAirFriction(float airFriction);

    private:
        bool isKnown(const PhysicsObject *object) const;
        std::vector<PhysicsObject*> m_objectsList;

        Box m_worldBox; ///< World box
        float m_gravity; ///< Gravity force in pixels / second squared
        float m_airFriction; ///< Air friction in pixels / second squared
        std::string m_groundMaterial; ///< Name of the ground material
};

#endif
