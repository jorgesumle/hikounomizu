/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PlaylistPlayer.hpp"
#include "GUI/PlaylistNotifier.hpp" ///< Playlista

#include "Audio/Playlist.hpp" ///< SongData
#include "Tools/Timer.hpp"
#include <thread>
#include <mutex>
#include <vector>

namespace
{
    const std::vector<SongData> SONGS =
    {
        SongData("Cool Cool Mountain", "David Kvistorf",
                 "audio/music/cool_cool_mountain.ogg"),
        SongData("Underwater Battle", "David Kvistorf",
                 "audio/music/underwater_battle.ogg"),
        SongData("Arukas Bloom", "TAD",
                 "audio/music/arukas_bloom.ogg"),
        SongData("Blueberries", "Viktor Kraus",
                 "audio/music/blueberries.ogg"),
        SongData("Outer Space", "wi-photos",
                 "audio/music/outer_space.ogg"),
        SongData("Winter Wind", "wi-photos",
                 "audio/music/winter_wind.ogg"),
        SongData("Teaser Background Music", "Migfus20",
                 "audio/music/teaser_background_music.ogg"),
    };

    struct ThreadData
    {
        PlaylistNotifiable *notifier;
        float volume;
        bool next, stop, updateVolume;
    };

    ThreadData threadData;
    std::mutex dataMutex;
    std::thread *thread = nullptr;

    void threadNotify(PlaylistNotifiable *notifier, const Playlist &playlist)
    {
        if (notifier != nullptr)
            notifier->notify(playlist.getData().author,
                             playlist.getData().title);
    }

    void threadMain()
    {
        //Start playing music
        Playlist playlist(SONGS);
        {
            std::lock_guard<std::mutex> lock(dataMutex);
            playlist.setVolume(threadData.volume);
        }

        while (1)
        {
            playlist.update();

            {
                std::lock_guard<std::mutex> lock(dataMutex);
                if (threadData.stop)
                    return; //Stop requested from the main thread

                if (playlist.songEnded())
                {
                    if (!playlist.playNext())
                        return; //No songs could be loaded

                    threadNotify(threadData.notifier, playlist);
                }

                if (threadData.next)
                {
                    playlist.stopSong();
                    threadData.next = false;
                }

                if (threadData.updateVolume)
                {
                    playlist.setVolume(threadData.volume);
                    threadData.updateVolume = false;
                }
            }

            Timer::sleep(Timer::duration_float_ms(10.f));
        }
    }
}

void PlaylistPlayer::start(PlaylistNotifiable *notifier, float volume)
{
    if (thread != nullptr)
        return;

    {
        std::lock_guard<std::mutex> lock(dataMutex);
        threadData.notifier = notifier;
        threadData.volume = volume;
        threadData.next = false;
        threadData.stop = false;
        threadData.updateVolume = false;
    }

    //Create & register thread
    thread = new std::thread(threadMain);
}

void PlaylistPlayer::setNotifier(PlaylistNotifiable *notifier)
{
    std::lock_guard<std::mutex> lock(dataMutex);
    threadData.notifier = notifier;
}

void PlaylistPlayer::setVolume(float volume)
{
    std::lock_guard<std::mutex> lock(dataMutex);
    threadData.volume = volume;
    threadData.updateVolume = true;
}

void PlaylistPlayer::nextSong()
{
    std::lock_guard<std::mutex> lock(dataMutex);
    threadData.next = true;
}

void PlaylistPlayer::stop()
{
    {
        std::lock_guard<std::mutex> lock(dataMutex);
        threadData.stop = true;
    }

    if (thread != nullptr)
    {
        thread->join();
        delete thread;
        thread = nullptr;
    }
}
