/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ANIMATION
#define DEF_ANIMATION

#include "Structs/Box.hpp"
#include <cstddef>
#include <vector>
#include <string>

struct Frame
{
    Frame() {}

    Frame(const Box &source, const Box &body) : srcBox(source), bodyBox(body)
    {

    }

    Box srcBox, bodyBox;
};

class Animation
{
    public:
        Animation();
        virtual ~Animation() = default;

        void addFrame(const Frame &frame);

        /// Reset the animation to its initial state
        virtual void reset();
        virtual void goToTime(float time);

        /// Go forward in time by \p distance time units, stops at the end state
        void goForward(float distance);

        /// Go forward in time by \p distance time units, if the end state was
        /// reached, returns true and adds the remaining distance from the start
        bool goInLoop(float distance);

        std::size_t getFrameSize() const;

        void setPath(const std::string &path);
        const std::string &getPath() const;

        void setDuration(float duration);
        float getDuration() const;

        void setScale(float scale);
        float getXScale() const;
        float getYScale() const;

        float getCurrentTime() const;
        std::size_t getCurrentFrame() const;

        bool atInitialState() const;
        bool atEndedState() const;

        bool getSourceBox(Box &target) const;
        bool getBodyBox(Box &target) const;

        float getWidth() const;
        float getHeight() const;

    protected:
        std::vector<Frame> m_framesList;

        std::size_t m_frameIx;

        std::string m_path; ///< Relative path to the animation texture file
        float m_duration; ///< Duration in seconds
        float m_time; ///< Current time in the animation in seconds
        float m_xScale, m_yScale; ///< Scaling factor for each frame of the animation
};

#endif
