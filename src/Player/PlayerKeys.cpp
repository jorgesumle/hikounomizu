/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PlayerKeys.hpp"
#include "Tools/Input/Joystick.hpp"

PlayerKeys::PlayerKeys() :
m_punch(KeyboardKey(SDL_SCANCODE_Q)), m_kick(KeyboardKey(SDL_SCANCODE_W)),
m_throwWeapon(KeyboardKey(SDL_SCANCODE_E)), m_jump(KeyboardKey(SDL_SCANCODE_T)),
m_moveLeft(KeyboardKey(SDL_SCANCODE_F)), m_moveRight(KeyboardKey(SDL_SCANCODE_H)),
m_crouch(KeyboardKey(SDL_SCANCODE_G))
{

}

PlayerKeys::PlayerKeys(const Key &punch,
                       const Key &kick,
                       const Key &throwWeapon,
                       const Key &jump,
                       const Key &moveLeft,
                       const Key &moveRight,
                       const Key &crouch) :
m_punch(punch), m_kick(kick), m_throwWeapon(throwWeapon), m_jump(jump),
m_moveLeft(moveLeft), m_moveRight(moveRight), m_crouch(crouch)
{

}

void PlayerKeys::setPunchKey(const Key &punchKey)
{
    m_punch = punchKey;
}

const Key &PlayerKeys::getPunchKey() const
{
    return m_punch;
}

void PlayerKeys::setKickKey(const Key &kickKey)
{
    m_kick = kickKey;
}

const Key &PlayerKeys::getKickKey() const
{
    return m_kick;
}

void PlayerKeys::setThrowWeaponKey(const Key &throwWeaponKey)
{
    m_throwWeapon = throwWeaponKey;
}

const Key &PlayerKeys::getThrowWeaponKey() const
{
    return m_throwWeapon;
}

void PlayerKeys::setJumpKey(const Key &jumpKey)
{
    m_jump = jumpKey;
}

const Key &PlayerKeys::getJumpKey() const
{
    return m_jump;
}

void PlayerKeys::setMoveLeftKey(const Key &moveLeftKey)
{
    m_moveLeft = moveLeftKey;
}

const Key &PlayerKeys::getMoveLeftKey() const
{
    return m_moveLeft;
}

void PlayerKeys::setMoveRightKey(const Key &moveRightKey)
{
    m_moveRight = moveRightKey;
}

const Key &PlayerKeys::getMoveRightKey() const
{
    return m_moveRight;
}

void PlayerKeys::setCrouchKey(const Key &crouchKey)
{
    m_crouch = crouchKey;
}

const Key &PlayerKeys::getCrouchKey() const
{
    return m_crouch;
}

bool PlayerKeys::allKeysUndefined() const
{
    return (m_punch.isUndefined() &&
            m_kick.isUndefined() &&
            m_throwWeapon.isUndefined() &&
            m_jump.isUndefined() &&
            m_moveLeft.isUndefined() &&
            m_moveRight.isUndefined() &&
            m_crouch.isUndefined());
}

PlayerKeys PlayerKeys::undefinedKeys()
{
    return PlayerKeys(Key(), Key(), Key(), Key(), Key(), Key(), Key());
}

PlayerKeys PlayerKeys::defaultKeys(const Joystick &joystick)
{
    return PlayerKeys(joystick.getKeyFor(SDL_CONTROLLER_BUTTON_X),
                      joystick.getKeyFor(SDL_CONTROLLER_BUTTON_Y),
                      joystick.getKeyFor(SDL_CONTROLLER_BUTTON_B),
                      joystick.getKeyFor(SDL_CONTROLLER_BUTTON_A),
                      joystick.getKeyFor(SDL_CONTROLLER_BUTTON_DPAD_LEFT),
                      joystick.getKeyFor(SDL_CONTROLLER_BUTTON_DPAD_RIGHT),
                      joystick.getKeyFor(SDL_CONTROLLER_BUTTON_DPAD_DOWN));
}
