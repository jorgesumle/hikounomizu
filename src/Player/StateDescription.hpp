/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_STATE_DESCRIPTION
#define DEF_STATE_DESCRIPTION

#include <cstdint>

/// Identifies a state of a player,
/// a state has several sub attacks to perform
enum class StateIndex : std::uint8_t
{
    None = 0,
    Default = 1,
    JumpTransition = 2,
    Jumping = 3,
    Moving = 4,
    Crouched = 5,
    Falling = 6,
    KO = 7
};

/// Identifies an attack of a player,
/// specific to each state
enum class AttackIndex : std::uint8_t
{
    Default = 0,
    Punch = 1,
    Kick = 2,
    TakeAHit = 3
};

/// Identifies a moving state, i.e.,
/// whether there is a movement for a player, and a moving direction
enum class MovingState : std::uint8_t
{
    NotMoving = 0,
    MovingLeft = 1,
    MovingRight = 2
};

namespace StateDescription
{
    enum IndexLimit : std::uint8_t
    {
        State = static_cast<std::uint8_t>(StateIndex::KO),
        Attack = static_cast<std::uint8_t>(AttackIndex::TakeAHit),
        Moving = static_cast<std::uint8_t>(MovingState::MovingRight),
    };

    std::uint8_t writeStateIndex(StateIndex stateIx);
    StateIndex readStateIndex(std::uint8_t stateIx);

    std::uint8_t writeAttackIndex(AttackIndex attackIx);
    AttackIndex readAttackIndex(std::uint8_t attackIx);

    std::uint8_t writeMovingState(MovingState movingState);
    MovingState readMovingState(std::uint8_t movingState);
}

#endif
