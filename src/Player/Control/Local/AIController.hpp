/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_AI_CONTROLLER
#define DEF_AI_CONTROLLER

#include "Player/Control/PlayerController.hpp"
#include "Player/Player.hpp" ///< PlayerCallbackReceiver
#include "Tools/Timer.hpp"
#include <cstddef>
#include <random>
#include <vector>
#include <string>

class AIController : public PlayerController, public PlayerCallbackReceiver
{
    enum class Behaviour { Defensive, Offensive };

    public:
        AIController();

        void attach(Player &player) override;

        void act() override;
        void playerWasHit(float /*strength*/, HitDirection /*side*/,
                          const Player *source) override;
        void playerHitTriggered() override;

        void setEnemies(const std::vector<const Player*> &enemies);

    private:
        void randomDefensive(); ///< Act defensively (random actions)
        void removeKoEnemies(); ///< Remove all KO enemies from m_enemies
        void updateTargetEnemy(); ///< Randomly choose a target enemy

        std::mt19937 m_generator;
        std::uniform_int_distribution<int> m_actionDist;

        std::vector<const Player*> m_enemies; ///< All enemies
        const Player *m_targetEnemy; ///< The enemy the AI is focusing on

        Behaviour m_behaviour;

        /// Starting time of the last state
        Timer::TimePoint m_attackState_start;

        /// When behaving offensively, give enemies a break sometimes ;)
        bool m_attackPause;

        /// Starting time of the last pause
        Timer::TimePoint m_attackPause_start;
};

#endif
