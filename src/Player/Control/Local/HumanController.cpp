/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "HumanController.hpp"
#include "Player/Player.hpp"

HumanController::HumanController() : InputReadingController()
{

}

void HumanController::act()
{
    //Update moves
    if (m_player != nullptr && m_player->getAttack() == AttackIndex::Default &&
        !m_player->isKo() && !m_player->isHit())
    {
        if (isMoveLeftDown())
            m_player->setMoving(MovingState::MovingLeft);
        else if (isMoveRightDown())
            m_player->setMoving(MovingState::MovingRight);
        else
        {
            m_player->setMoving(MovingState::NotMoving);
            if (isCrouchDown())
            {
                if (m_player->getState() != StateIndex::Crouched)
                    m_player->crouch();
            }
            else if (m_player->getState() == StateIndex::Crouched)
                m_player->uncrouch(); //Go back to default state
        }
    }
}

void HumanController::punchPressed()
{
    //m_player is guaranteed to be valid by InputReadingController
    m_player->punch();
}

void HumanController::kickPressed()
{
    //m_player is guaranteed to be valid by InputReadingController
    m_player->kick();
}

void HumanController::throwPressed()
{
    //m_player is guaranteed to be valid by InputReadingController
    m_player->throwShuriken();
}

void HumanController::jumpPressed()
{
    //m_player is guaranteed to be valid by InputReadingController
    m_player->jump();
}
