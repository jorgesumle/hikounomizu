/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "NetClientController.hpp"
#include "Player/Player.hpp"
#include "Network/Serialization/PlayerStatus.hpp"
#include "Network/Serialization/PlayerAction.hpp"

NetClientController::NetClientController(PlayerActionSender *actionSender,
                                         const ClientInputState &initialState) :
InputReadingController(initialState.moveLeftDown,
                       initialState.moveRightDown,
                       initialState.crouchDown),
m_lastKnownMoveLeft(initialState.lastMoveLeft),
m_lastKnownMoveRight(initialState.lastMoveRight),
m_lastKnownCrouch(initialState.lastCrouch),
m_actionSender(actionSender)
{

}

void NetClientController::act()
{
    if (m_actionSender != nullptr && m_player != nullptr &&
        m_player->getAttack() == AttackIndex::Default &&
        !m_player->isKo() && !m_player->isHit())
    {
        const bool moveLeft = isMoveLeftDown(), moveRight = isMoveRightDown();
        const bool crouch = isCrouchDown();

        //The pressed keys changed
        if (m_lastKnownMoveLeft != moveLeft ||
            m_lastKnownMoveRight != moveRight ||
            m_lastKnownCrouch != crouch)
        {
            if (moveLeft)
                m_actionSender->sendPlayerAction(PlayerAction::Index::MoveLeft);
            else if (moveRight)
                m_actionSender->sendPlayerAction(PlayerAction::Index::MoveRight);
            else
            {
                if (m_lastKnownMoveLeft || m_lastKnownMoveRight)
                    m_actionSender->sendPlayerAction(PlayerAction::Index::StopMoving);

                if (crouch)
                    m_actionSender->sendPlayerAction(PlayerAction::Index::Crouch);
                else
                    m_actionSender->sendPlayerAction(PlayerAction::Index::Uncrouch);
            }

            m_lastKnownCrouch = crouch;
            m_lastKnownMoveLeft = moveLeft;
            m_lastKnownMoveRight = moveRight;
        }
    }
}

void NetClientController::playerStatusReceived(const PlayerStatus &status)
{
    if (m_player != nullptr)
        m_player->applyPlayerStatus(status);
}

void NetClientController::punchPressed()
{
    if (m_actionSender != nullptr)
        m_actionSender->sendPlayerAction(PlayerAction::Index::Punch);
}

void NetClientController::kickPressed()
{
    if (m_actionSender != nullptr)
        m_actionSender->sendPlayerAction(PlayerAction::Index::Kick);
}

void NetClientController::throwPressed()
{
    if (m_actionSender != nullptr)
        m_actionSender->sendPlayerAction(PlayerAction::Index::ThrowShuriken);
}

void NetClientController::jumpPressed()
{
    if (m_actionSender != nullptr)
        m_actionSender->sendPlayerAction(PlayerAction::Index::Jump);
}

ClientInputState NetClientController::getInputState() const
{
    return ClientInputState(m_lastKnownMoveLeft,
                            m_lastKnownMoveRight,
                            m_lastKnownCrouch,
                            InputReadingController::isMoveLeftDown(),
                            InputReadingController::isMoveRightDown(),
                            InputReadingController::isCrouchDown());
}
