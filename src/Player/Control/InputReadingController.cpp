/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "InputReadingController.hpp"
#include "Player/Player.hpp"

InputReadingController::InputReadingController(bool moveLeftDown,
                                               bool moveRightDown,
                                               bool crouchedDown) :
PlayerController(), m_moveLeftDown(moveLeftDown),
m_moveRightDown(moveRightDown), m_crouchedDown(crouchedDown)
{

}

void InputReadingController::keyEvent(const SDL_Event &event)
{
    //Key state on event
    if (m_player != nullptr && !m_player->isKo() && !m_player->isHit())
    {
        //Other actions
        if (m_inputMonitor.wasPressed(event, m_keys.getPunchKey(), m_deviceID))
            punchPressed();
        else if (m_inputMonitor.wasPressed(event, m_keys.getKickKey(), m_deviceID))
            kickPressed();
        else if (m_inputMonitor.wasPressed(event, m_keys.getThrowWeaponKey(), m_deviceID))
            throwPressed();
        else if (m_inputMonitor.wasPressed(event, m_keys.getJumpKey(), m_deviceID))
            jumpPressed();
    }
}

void InputReadingController::updateKeyState(const SDL_Event &event)
{
    if (m_inputMonitor.wasPressed(event, m_keys.getMoveLeftKey(), m_deviceID))
        m_moveLeftDown = true;
    else if (m_inputMonitor.wasReleased(event, m_keys.getMoveLeftKey(), m_deviceID))
        m_moveLeftDown = false;

    if (m_inputMonitor.wasPressed(event, m_keys.getMoveRightKey(), m_deviceID))
        m_moveRightDown = true;
    else if (m_inputMonitor.wasReleased(event, m_keys.getMoveRightKey(), m_deviceID))
        m_moveRightDown = false;

    if (m_inputMonitor.wasPressed(event, m_keys.getCrouchKey(), m_deviceID))
        m_crouchedDown = true;
    else if (m_inputMonitor.wasReleased(event, m_keys.getCrouchKey(), m_deviceID))
        m_crouchedDown = false;

    m_inputMonitor.update(event, m_deviceID);
}

bool InputReadingController::isMoveLeftDown() const
{
    return m_moveLeftDown;
}

bool InputReadingController::isMoveRightDown() const
{
    return m_moveRightDown;
}

bool InputReadingController::isCrouchDown() const
{
    return m_crouchedDown;
}

void InputReadingController::setDeviceID(int id)
{
    m_deviceID = static_cast<Uint8>(id);
}

void InputReadingController::setKeys(const PlayerKeys &keys)
{
    m_keys = keys;
}
