/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_CONTROLLER
#define DEF_PLAYER_CONTROLLER

#include <cstdint>
#include <list>

class Player;

/// Make a player act based on external input
/// (e.g., keyboard/controller for local human player, or AI logic)
class PlayerController
{
    public:
        PlayerController();
        virtual ~PlayerController();

        virtual void attach(Player &player);
        virtual void act() = 0;

    protected:
        Player *m_player;
};

#endif
