/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_LOG
#define DEF_LOG

#include <string>
#include "Structs/Color.hpp"

namespace Log
{
    class Decoration
    {
        public:
            enum class Emphasis {None, Bold, Faint, Italic};

            Decoration(Emphasis emph);
            Decoration(const Color &color, Emphasis emph=Emphasis::None);
            Decoration(const Color &fg, const Color &bg, Emphasis emph=Emphasis::None);

            std::string apply(const std::string &str) const;

        private:
            enum class ColorMode {None, Foreground, FgAndBg};

            void generateANSI();
            std::string m_ansiCodes;

            Color m_color1, m_color2;
            ColorMode m_mode;
            Emphasis m_emphasis;
    };

    /// Logs information as a raw string
    void info(const std::string &info);

    /// Logs information with a specific decoration
    void info(const std::string &info, const Decoration &style);

    /// Logs error messages
    void err(const std::string &error);
}

#endif
