/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_KEYBOARD_KEY
#define DEF_KEYBOARD_KEY

#include <SDL2/SDL.h>
#include <string>

class KeyboardKey
{
    public:
        KeyboardKey();

        /// Construct the keyboard key from a layout-independent scancode
        explicit KeyboardKey(SDL_Scancode keyboardKey);

        bool keyPressed(const SDL_Event &event) const;
        bool keyReleased(const SDL_Event &event) const;

        std::string getName() const;
        std::string getKeyCode() const;

        /// Construct the keyboard key from a layout-dependent keycode
        static KeyboardKey layoutDependent(SDL_Keycode keycode);

    private:
        SDL_Scancode m_key; ///< The keyboard key
};

#endif
