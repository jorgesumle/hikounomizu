/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_INPUT_MONITOR
#define DEF_INPUT_MONITOR

#include "JoyHatKey.hpp" ///< HatState
#include "JoyAxisKey.hpp" ///< AxisState
#include <SDL2/SDL.h>
#include <map>

class Key;

class InputMonitor
{
    public:
        void update(const SDL_Event &event, Uint8 deviceID);

        bool wasPressed(const SDL_Event &event, const Key &key,
                        Uint8 deviceID) const;
        bool wasReleased(const SDL_Event &event, const Key &key,
                         Uint8 deviceID) const;

    private:
         std::map<Uint8, HatState> m_previousHat;
         std::map<Uint8, AxisState> m_previousAxis;
};

#endif
