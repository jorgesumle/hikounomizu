/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_KEY
#define DEF_KEY

#define DEVICE_KEYBOARD -1

#include "KeyboardKey.hpp"
#include "JoyButtonKey.hpp"
#include "JoyHatKey.hpp"
#include "JoyAxisKey.hpp"

class Key
{
    public:
        enum class Type { Undefined, Keyboard, JoyButton, JoyHat, JoyAxis };

        Key();
        Key(const KeyboardKey &keyboardKey);
        Key(const JoyButtonKey &joyButtonKey);
        Key(const JoyHatKey &joyHatKey);
        Key(const JoyAxisKey &joyAxisKey);

        /// Check if the key is a button pressed from an event
        /// (keyboard and joystick buttons)
        bool buttonPressed(const SDL_Event &event,
                           int deviceID = DEVICE_KEYBOARD) const;
        /// Check if the key is a hat pressed from an event (joystick hat)
        bool hatPressed(const SDL_Event &event, int deviceID,
            const std::map<Uint8, HatState> &previousHatStates) const;
        /// Check if the key is an axis pressed from an event (joystick axis)
        bool axisPressed(const SDL_Event &event, Uint8 deviceID,
            const std::map<Uint8, AxisState> &previousAxisStates) const;

        /// Check if the key is a button released from an event
        /// (keyboard and joystick buttons)
        bool buttonReleased(const SDL_Event &event,
                            int deviceID = DEVICE_KEYBOARD) const;
        /// Check if the key is a hat released from an event (joystick hat)
        bool hatReleased(const SDL_Event &event, int deviceID,
            const std::map<Uint8, HatState> &previousHatStates) const;
        /// Check if the key is an axis released from an event (joystick axis)
        bool axisReleased(const SDL_Event &event, Uint8 deviceID,
            const std::map<Uint8, AxisState> &previousAxisStates) const;

        std::string getName() const;
        std::string getKeyCode() const;

        bool isFromKeyboard() const;
        bool isFromJoystick() const;
        bool isUndefined() const;
        Type getType() const;

        /// Read as a Key the pressed key described in a SDL_Event
        static bool pressedFromEvent(const SDL_Event &event, Key &dstKey,
                                     int &dstDeviceID);

        /// Read as a Key the key described in a Hikou no mizu key code
        static Key fromKeyCode(const std::string &code);

    private:
        Type m_type;
        union
        {
            KeyboardKey m_keyboardKey; ///< A keyboard key
            JoyButtonKey m_joyButtonKey; ///< A joystick key
            JoyHatKey m_joyHatKey; ///< A joystick hat key
            JoyAxisKey m_joyAxisKey; ///< A joystick axis key
        };
};

#endif
