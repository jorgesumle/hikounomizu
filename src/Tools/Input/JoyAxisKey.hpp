/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_JOYAXIS_KEY
#define DEF_JOYAXIS_KEY

#include <SDL2/SDL.h>
#include <map>
#include <string>

enum class JoyAxisMove { Negative, Positive };
enum class AxisState { Negative, Centered, Positive };

class JoyAxisKey
{
    public:
        JoyAxisKey();
        JoyAxisKey(Uint8 axisID, JoyAxisMove axisMove);

        bool keyPressed(const SDL_Event &event, Uint8 deviceID,
            const std::map<Uint8, AxisState> &previousAxisStates) const;
        bool keyReleased(const SDL_Event &event, Uint8 deviceID,
            const std::map<Uint8, AxisState> &previousAxisStates) const;

        std::string getName() const;
        std::string getKeyCode() const;

        static AxisState axisStateFromSDLCode(Sint16 sdlValue);

        static JoyAxisMove axisMoveFromStringCode(const std::string &stringCode);
        static std::string axisMoveStringCode(const JoyAxisMove &code);

    private:
        Uint8 m_axisID;
        JoyAxisMove m_axisMove;
};
#endif
