/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "JoyButtonKey.hpp"

#include "Tools/Format.hpp"
#include "Tools/I18n.hpp" ///< _ macro for i18n
#include <sstream>

JoyButtonKey::JoyButtonKey() : JoyButtonKey(0) {}

JoyButtonKey::JoyButtonKey(Uint8 joyKey) : m_joyKey(joyKey)
{

}

bool JoyButtonKey::keyPressed(const SDL_Event &event, int deviceID) const
{
    return (event.type == SDL_JOYBUTTONDOWN &&
            deviceID == event.jbutton.which &&
            m_joyKey == event.jbutton.button);
}

bool JoyButtonKey::keyReleased(const SDL_Event &event, int deviceID) const
{
    return (event.type == SDL_JOYBUTTONUP &&
            deviceID == event.jbutton.which &&
            m_joyKey == event.jbutton.button);
}

std::string JoyButtonKey::getName() const
{
    return _f(KeyJoyButton, std::make_pair("button_id", +m_joyKey + 1));
}

std::string JoyButtonKey::getKeyCode() const
{
    std::ostringstream code;
    code << "jb" << +m_joyKey;
    return code.str();
}
