/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Filesystem.hpp"

#if defined(__unix__) || defined(__APPLE__)
    #include <cstdlib>
    #include <sys/stat.h>

#elif defined(__HAIKU__)
    #include <Path.h>
    #include <Directory.h>
    #include <FindDirectory.h>

#elif defined(_WIN32)
    #include <windows.h>
    #include <shlobj.h>

#endif

namespace Filesystem
{
    void makeDirectory(const std::string &path)
    {
        #if defined(__unix__) || defined(__APPLE__)
            mkdir(path.c_str(), S_IRWXU); //chmod 0700

        #elif defined(__HAIKU__)
            create_directory(path.c_str(), S_IRWXU);

        #elif defined(_WIN32)
            CreateDirectory(path.c_str(), nullptr);

        #endif
    }

    bool getHNMHome(std::string &dstPath)
    {
        #if defined(__unix__)
            //Use $XDG_CONFIG_HOME or $HOME/.config by default as a base path
            if (const char *xdg_home = std::getenv("XDG_CONFIG_HOME"))
            {
                dstPath = std::string(xdg_home) + "/hikounomizu/";
                return true;
            }
            else if (const char *home = std::getenv("HOME"))
            {
                std::string configDirectory = std::string(home) + "/.config";
                makeDirectory(configDirectory); //Create .config if needed
                dstPath = configDirectory + "/hikounomizu/";
                return true;
            }

        #elif defined(__APPLE__)
            if (const char *home = std::getenv("HOME"))
            {
                dstPath = std::string(home) +
                          "/Library/Application Support/hikounomizu/";
                return true;
            }

        #elif defined(__HAIKU__)
            BPath path;
            if (find_directory(B_USER_SETTINGS_DIRECTORY, &path, true) == B_NO_ERROR)
            {
                dstPath = std::string(path.Path()) + "/HikouNoMizu/";
                return true;
            }

        #elif defined(_WIN32)
            char szPath[MAX_PATH];
            if (SUCCEEDED(SHGetFolderPathA(nullptr, CSIDL_APPDATA, nullptr, 0, szPath)))
            {
                dstPath = std::string(szPath) + "\\hikounomizu\\";
                return true;
            }

        #endif

        return false;
    }

    std::string getBasename(const std::string &path)
    {
        std::size_t basestart =
        #ifdef _WIN32
            path.find_last_of('\\');
        #else
            path.find_last_of('/');
        #endif

        return (basestart != std::string::npos) ? path.substr(basestart + 1)
                                                : path;
    }
}
