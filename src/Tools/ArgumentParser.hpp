/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ARGUMENT_PARSER
#define DEF_ARGUMENT_PARSER

#include "Tools/Log.hpp"
#include "Tools/Format.hpp"
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>

/// Option with optionally an associated value that was read from ArgumentParser
struct ParsedOption
{
    explicit ParsedOption(std::string &&optionName) :
    name(std::move(optionName)) {}

    std::string name;
    std::string value;
};

/// Functor to find a parsed option by name in a ParsedOption container
struct FindParsedOption
{
    explicit FindParsedOption(const std::string &optionName) :
    name(optionName) {}

    FindParsedOption(const std::string&&) = delete;

    bool operator() (const ParsedOption &option) const
    {
        return option.name == name;
    }

    const std::string &name;
};

/// List of ParsedOption found by ArgumentParser with utility methods
/// to check option existence and retrieve typed values
struct ParsedOptions
{
    ParsedOptions() : hadError(false) {}

    bool hasOption(const std::string &name) const
    {
        return std::find_if(options.begin(), options.end(),
                    FindParsedOption(name)) != options.end();
    }

    bool hasOption(const std::string &name, std::string &dstValue) const
    {
        std::vector<ParsedOption>::const_iterator it =
            std::find_if(options.begin(), options.end(),
                         FindParsedOption(name));

        if (it != options.end())
        {
            dstValue = it->value;
            return true;
        }

        return false;
    }

    template <typename T>
    bool hasOption(const std::string &name, T &dst) const
    {
        std::vector<ParsedOption>::const_iterator it =
            std::find_if(options.begin(), options.end(),
                         FindParsedOption(name));

        if (it == options.end())
            return false;

        T typedItem;
        std::istringstream convertToType(it->value);
        convertToType >> typedItem;

        if (convertToType.fail())
        {
            Log::err(Format::format("Invalid value '{}' for the option: {}",
                                    it->value, it->name));
            return false;
        }

        dst = std::move(typedItem);
        return true;
    }

    std::vector<ParsedOption> options;
    /// Whether any unexpected input was encountered while parsing
    bool hadError;
};

/// Utility to parse and read a set of expected parameters
/// from the command line arguments
class ArgumentParser
{
    public:
        void addShortOption(char option, bool expectValue = false);
        void addLongOption(const std::string &option, bool expectValue = false);

        /// Adds an option that has both a short and long name alternative
        void addOption(char shortName, const std::string &longName,
                       bool expectValue = false);

        /// Parses the command line arguments, return the options found
        /// which match the expected arguments
        ParsedOptions parse(int argc, char **argv) const;

    private:
        /// Expected option string, e.g., "-h" or "--help"
        /// and whether it expects an associated value
        std::map<std::string, bool> m_expectedOptions;
};

#endif
