/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_TIMER
#define DEF_TIMER

#include <chrono>
#include <cstdint>

class Timer
{
    public:
        using TimePoint = std::chrono::time_point<std::chrono::steady_clock>;
        using duration_float_ms = std::chrono::duration<float, std::milli>;
        using duration_uint64_us = std::chrono::duration<std::uint64_t, std::micro>;

        Timer();

        void reset(); ///< Reset the timer such that getTicks() == 0

        /// Reset the timer such that getTicks() == \p duration
        void resetToElapsed(const duration_uint64_us &duration);

        /// Get the elapsed time in ms since the last reset(),
        /// using Timer::getTime() as the current time
        float getElapsed() const;

        /// Get the elapsed time in duration_uint64_us since the last reset(),
        /// using \p now as a reference for the current time
        duration_uint64_us getTicks(const TimePoint &now = getTime()) const;

        /// Sleep until more than \p duration has passed since m_previousTime
        void waitUntilElapsed(const duration_float_ms &duration) const;

        /// Sleep for \p duration
        static void sleep(const duration_float_ms &duration);
        static TimePoint getTime();

    private:
        TimePoint m_previousTime;
};

#endif
