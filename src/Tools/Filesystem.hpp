/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_FILESYSTEM
#define DEF_FILESYSTEM

#include <string>

/// Cross-platform functions to get filesystem information and make directories
namespace Filesystem
{
    /// Attempt to create a directory at a given path
    void makeDirectory(const std::string &path);

    /// Attempt to retrieve Hikou no mizu's os-dependent home config directory
    bool getHNMHome(std::string &dstPath);

    /// Returns the basename of a path to a file
    std::string getBasename(const std::string &path);
}

#endif
