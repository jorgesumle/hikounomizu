/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SERVER_CONFIGURATION
#define DEF_SERVER_CONFIGURATION

#include "Fight/FightRules.hpp"
#include "Network/Serialization/MatchmakingStrategy.hpp"
#include "Tools/Utf8.hpp"
#include <chrono>
#include <vector>
#include <string>
#include <cstdint>
#include <cstddef>

class ServerConfiguration
{
    public:
        ServerConfiguration();
        bool load(const std::string &path);
        bool save(const std::string &path);

        void setPort(std::uint16_t port);
        std::uint16_t getPort() const;

        MatchmakingStrategy::Type getMatchmakingStrategy() const;
        const FightRules &getFightRules() const;
        const std::vector<std::string> &getArenas() const;
        std::size_t getSpectatorLimit() const;

        std::chrono::seconds getInactivityTimeoutLobby() const;
        std::chrono::seconds getInactivityTimeoutInGame() const;

        const Utf8::utf8_string &getWelcomeMessage() const;

    private:
        std::uint16_t m_port;
        MatchmakingStrategy::Type m_matchmaking;
        FightRules m_fightRules;
        std::vector<std::string> m_arenas;
        std::size_t m_spectatorLimit;
        std::chrono::seconds m_inactivityLobby, m_inactivityInGame;
        Utf8::utf8_string m_welcome;
};

#endif
