/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ConfWriter.hpp"

#ifndef CONF_DISABLE_PLAYERKEYS
    #include "Player/PlayerKeys.hpp"
#endif

#include "Tools/Log.hpp"
#include "Tools/Format.hpp"
#include <fstream>

void ConfWriter::addItem(const std::string &name, const std::string &value)
{
    m_confItems.push_back( std::make_pair(name, value) );
}

#ifndef CONF_DISABLE_PLAYERKEYS
    void ConfWriter::addPlayerKeysList(const PlayerKeys &value)
    {
        //Set elements
        addItem("punch", value.getPunchKey().getKeyCode());
        addItem("kick", value.getKickKey().getKeyCode());
        addItem("throw_weapon", value.getThrowWeaponKey().getKeyCode());
        addItem("jump", value.getJumpKey().getKeyCode());
        addItem("move_left", value.getMoveLeftKey().getKeyCode());
        addItem("move_right", value.getMoveRightKey().getKeyCode());
        addItem("crouch", value.getCrouchKey().getKeyCode());
    }
#endif

bool ConfWriter::writeFile(const std::string &filePath) const
{
    //Open file
    std::ofstream file;
    file.open(filePath.c_str(), std::ios::out | std::ios::trunc);

    if (file.fail())
        return false;

    std::vector< std::pair<std::string, std::string> >::const_iterator it;
    for (it = m_confItems.begin(); it != m_confItems.end(); ++it)
        file << it->first << ' ' << it->second << '\n';

    file.close();
    return true;
}
