/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_WINDOW
#define DEF_WINDOW

#include "Display.hpp"
#include "Viewport.hpp"
#include "Tools/Timer.hpp"
#include "Structs/Box.hpp"
#include <SDL2/SDL.h>
#include <string>
#include <vector>

struct Vector;

class Window
{
    public:
        Window();
        Window(const Window &copied) = delete;
        Window &operator=(const Window &copied) = delete;

        bool create(const char *title, const Display::DisplayLayout &layout,
                Display::VSyncMode vsyncMode = Display::VSyncMode::OnAdaptive,
                int msBuffers = 1, int msSamples = 4);
        void destroy();

        void resizeWindow(const Display::DisplayLayout &layout);

        void initView(float viewWidth, float viewHeight);
        void resizeView();

        void clear();
        void flush();

        bool pollEvent(SDL_Event &event);

        //Get and set methods
        void setIcon(const std::string &iconPath);
        void setVSyncMode(Display::VSyncMode vsyncMode);
        void setFrameRate(float frameRate);

        Display::DisplayMode getDisplayMode() const;

        const Viewport &getViewport() const;
        Vector getViewSize() const;

        /// Get available display mode sizes
        std::vector<std::pair<int, int>> getDisplayModeSizes();

    private:
        bool applyFullscreenMode(int width, int height);

        SDL_Window *m_window;
        SDL_GLContext m_glContext;

        Viewport m_viewport;
        float m_latency;
        Timer m_time;

        Display::VSyncMode m_vsyncMode;
};

#endif
