/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_TEXTURE
#define DEF_TEXTURE

#include "Graphics/GL.hpp"
#include <string>

class Texture
{
    public:
        enum class MinMagFilter { Linear, Nearest };

        Texture();
        Texture(const Texture &copied) = delete;
        Texture &operator=(const Texture &copied) = delete;
        ~Texture();

        void draw(GLfloat x1 = 0.f, GLfloat y1 = 0.f,
                  GLfloat x2 = 1.f, GLfloat y2 = 1.f) const;

        GLsizei getWidth() const;
        GLsizei getHeight() const;

        bool loadFromFile(const std::string &relPath, MinMagFilter filter);
        bool loadFromMemory(const GLvoid *pixels, GLint format,
                            GLenum dataFormat, GLsizei width, GLsizei height,
                            MinMagFilter filter);

    private:
        GLuint m_glTexture;

        GLsizei m_width;
        GLsizei m_height;
};

#endif
