/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_GLYPH
#define DEF_GLYPH

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

#include "Texture.hpp"
#include "Structs/Vector.hpp"

class Glyph
{
    public:
        Glyph();
        Glyph(const Glyph &copied) = delete;
        Glyph &operator=(const Glyph &copied) = delete;

        void load(const FT_BitmapGlyph &data, const Vector &advance);
        void draw() const;

        const Vector &getAdvance() const;
        const Vector &getTopLeft() const;

        unsigned int getWidth() const;
        unsigned int getHeight() const;

    private:
        Texture m_texture;

        Vector m_advance, m_topLeft;
        unsigned int m_width, m_height;
};

#endif
