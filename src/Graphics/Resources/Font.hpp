/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_FONT
#define DEF_FONT

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

#include <map>
#include <string>

class Glyph;

class Font
{
    public:
        Font(const FT_Library library, const std::string &relPath, int charSize);
        Font(const Font &copied) = delete;
        Font &operator=(const Font &copied) = delete;
        ~Font();

        float getKerning(FT_ULong char1, FT_ULong char2);

        const Glyph *getGlyph(FT_ULong character);
        bool hasGlyph(FT_ULong character) const;
        int getCharSize() const;

    private:
        void loadGlyph(FT_ULong character);
        bool isGlyphLoaded(FT_ULong character);

        std::map<FT_ULong, Glyph*> m_glyphsList;
        int m_charSize;
        bool m_kerningAvailable;

        FT_Face m_face;
};

#endif
