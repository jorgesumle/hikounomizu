/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Polygon.hpp"

#include <cmath>

Polygon::Polygon() : Drawable(), m_borderSize(1.f) {}

void Polygon::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();

    //Enable needed modules
    #ifndef _WIN32
        glEnable(GL_MULTISAMPLE);
    #endif

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //Draw
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);

    glVertexPointer(2, GL_FLOAT, 0, &m_vertexArray[0]);

    //Background
    glColorPointer(4, GL_UNSIGNED_BYTE, 8 * sizeof(GLubyte), &m_colorsArray[0]);
    glDrawArrays(GL_POLYGON, 0, static_cast<GLsizei>(m_pointsData.size()));

    //Border
    if (m_borderSize > 0.f)
    {
        glLineWidth(m_borderSize);

        glColorPointer(4, GL_UNSIGNED_BYTE,
                       8 * sizeof(GLubyte), &m_colorsArray[4]);
        glDrawArrays(GL_LINE_LOOP, 0,
                     static_cast<GLsizei>(m_pointsData.size()));

        glLineWidth(1);
    }

    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

    //Disable loaded modules
    glDisable(GL_BLEND);

    #ifndef _WIN32
        glDisable(GL_MULTISAMPLE);
    #endif

    Drawable::popMatrix();
}

void Polygon::addPoint(float x, float y, const Color &color,
                       const Color &borderColor)
{
    m_pointsData.emplace_back(x, y, color, borderColor);

    updateVertexArray();
    updateColorsArray();
}

void Polygon::setUniformColor(const Color &color, const Color &borderColor)
{
    for (Point &point : m_pointsData)
    {
        point.color = color;
        point.borderColor = borderColor;
    }

    updateColorsArray();
}

void Polygon::setBorderSize(float borderSize)
{
    if (borderSize >= 0.f)
        m_borderSize = borderSize;
}

void Polygon::updateVertexArray()
{
    m_vertexArray.clear();
    m_vertexArray.reserve(m_pointsData.size() * 2);

    for (Point &point : m_pointsData)
    {
        m_vertexArray.push_back(point.x);
        m_vertexArray.push_back(point.y);
    }
}

void Polygon::updateColorsArray()
{
    m_colorsArray.clear();
    m_colorsArray.reserve(m_pointsData.size() * 8);

    for (Point &point : m_pointsData)
    {
        m_colorsArray.push_back(point.color.red);
        m_colorsArray.push_back(point.color.green);
        m_colorsArray.push_back(point.color.blue);
        m_colorsArray.push_back(point.color.alpha);

        m_colorsArray.push_back(point.borderColor.red);
        m_colorsArray.push_back(point.borderColor.green);
        m_colorsArray.push_back(point.borderColor.blue);
        m_colorsArray.push_back(point.borderColor.alpha);
    }
}

//Static methods
Polygon Polygon::rectangle(float width, float height,
                           const Color &color, const Color &borderColor)
{
    Polygon polygon;
    polygon.addPoint(0.f, 0.f, color, borderColor);
    polygon.addPoint(width, 0.f, color, borderColor);
    polygon.addPoint(width, height, color, borderColor);
    polygon.addPoint(0.f, height, color, borderColor);

    return polygon;
}

Polygon Polygon::circle(float radius, unsigned int precision,
                        const Color &color, const Color &borderColor)
{
    constexpr float PI = 3.14159265f;
    Polygon polygon;

    const float step = 2.f * PI / static_cast<float>(precision);
    float angle = 0.f;
    for (unsigned int i = 0; i < precision; i++)
    {
        polygon.addPoint( radius * static_cast<float>( cos(angle) ) + radius,
                          radius * static_cast<float>( sin(angle) ) + radius,
                          color, borderColor );
        angle += step;
    }

    return polygon;
}
