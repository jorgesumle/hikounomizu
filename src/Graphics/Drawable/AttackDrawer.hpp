/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ATTACK_DRAWER
#define DEF_ATTACK_DRAWER

#include "AnimationDrawer.hpp"
#include "Player/Attack.hpp" ///< HitBox
#include "Polygon.hpp"

class AttackDrawer : public Drawable
{
    public:
        AttackDrawer();
        void draw() override;
        void attachTo(const Attack &attack, Texture &texture,
                      bool drawHitboxes);

    private:
        void drawHitBoxes();

        struct HitBoxDrawer
        {
            HitBoxDrawer();
            HitBoxDrawer(const HitBox &hitbox, const Polygon &polygon) :
            hitbox(hitbox), polygon(polygon) {}

            HitBox hitbox;
            Polygon polygon;
        };

        const Attack *m_attack;

        AnimationDrawer m_animDrawer;
        std::vector<HitBoxDrawer> m_hitBoxes;
        bool m_drawHitboxes;
};

#endif
