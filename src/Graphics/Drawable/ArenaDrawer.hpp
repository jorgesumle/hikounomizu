/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ARENA_DRAWER
#define DEF_ARENA_DRAWER

#include "Sprite.hpp"
#include <vector>

class Arena;
class Platform;
class TextureManager;

/// Utility to draw an arena and its platforms
class ArenaDrawer
{
    public:
        void draw();

        void detach();
        void attachTo(const Arena &arena, TextureManager &textureManager);

    private:
        /// Utility to draw a platform
        class PlatformDrawer : public Drawable
        {
            public:
                PlatformDrawer() : m_platform(nullptr) {}
                void draw() override;
                void update();
                void attachTo(const Platform &platform, Texture &texture);

            private:
                const Platform *m_platform;
                Sprite m_sprite;
        };

        Sprite m_background;
        std::vector<PlatformDrawer> m_platformDraw;
};

#endif
