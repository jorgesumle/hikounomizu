/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_POLYGON
#define DEF_POLYGON

#include "Drawable.hpp"
#include "Structs/Color.hpp"
#include "Graphics/GL.hpp"
#include <cstddef>
#include <vector>

struct Point
{
    Point() : x(0.f), y(0.f) {}

    Point(float xCoords, float yCoords,
          const Color &background, const Color &border) :
    x(xCoords), y(yCoords), color(background), borderColor(border)
    {

    }

    float x, y;
    Color color, borderColor;
};

class Polygon : public Drawable
{
    public:
        Polygon();
        void draw() override;
        void addPoint(float x, float y, const Color &color,
                      const Color &borderColor);

        void setUniformColor(const Color &color,
                             const Color &borderColor = Color());
        void setBorderSize(float borderSize);

        /// Generate a rectangle of given \p width and \p height
        static Polygon rectangle(float width, float height,
                                 const Color &color = Color(),
                                 const Color &borderColor = Color());

        /// Generate a circle of radius \p radius
        static Polygon circle(float radius, unsigned int precision = 360,
                              const Color &color = Color(),
                              const Color &borderColor = Color());

    private:
        void updateVertexArray();
        void updateColorsArray();

        std::vector<float> m_vertexArray;
        std::vector<GLubyte> m_colorsArray;

        std::vector<Point> m_pointsData;

        float m_borderSize;
};

#endif
