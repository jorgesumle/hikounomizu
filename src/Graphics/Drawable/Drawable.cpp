/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Drawable.hpp"

#include "Graphics/GL.hpp"
#include <cmath>

Drawable::Drawable() :
m_lockToGrid(true),
m_xPosition(0.f), m_yPosition(0.f),
m_xOrigin(0.f), m_yOrigin(0.f),
m_xScale(1.f), m_yScale(1.f),
m_rotationAngle(0.f),
m_xFlip(false), m_yFlip(false)
{

}

Drawable::~Drawable()
{

}

void Drawable::pushMatrix()
{
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
}

void Drawable::updateMatrix()
{
    glTranslatef(m_xPosition, m_yPosition, 0.f);

    if (m_xFlip) glScalef(-1.f, 1.f, 1.f);
    if (m_yFlip) glScalef(1.f, -1.f, 1.f);
    glRotatef(m_rotationAngle, 0.f, 0.f, 1.f);
    glTranslatef(-m_xOrigin, -m_yOrigin, 0.f);

    glScalef(m_xScale, m_yScale, 1.f);
}

void Drawable::popMatrix()
{
    glPopMatrix();
}

//Get and set methods
    //Placement mode
void Drawable::lockToGrid(bool lock)
{
    m_lockToGrid = lock;
}

    //Position
void Drawable::setPosition(float x, float y)
{
    setXPosition(x);
    setYPosition(y);
}

void Drawable::setXPosition(float position)
{
    m_xPosition = m_lockToGrid ? floorf(position) : position;
}

float Drawable::getXPosition() const
{
    return m_xPosition;
}

void Drawable::setYPosition(float position)
{
    m_yPosition = m_lockToGrid ? floorf(position) : position;
}

float Drawable::getYPosition() const
{
    return m_yPosition;
}

    //Origin
void Drawable::setOrigin(float x, float y)
{
    setXOrigin(x);
    setYOrigin(y);
}

void Drawable::setXOrigin(float origin)
{
    m_xOrigin = m_lockToGrid ? floorf(origin) : origin;
}

float Drawable::getXOrigin() const
{
    return m_xOrigin;
}

void Drawable::setYOrigin(float origin)
{
    m_yOrigin = m_lockToGrid ? floorf(origin) : origin;
}

float Drawable::getYOrigin() const
{
    return m_yOrigin;
}

    //Scale
void Drawable::setScale(float x, float y)
{
    setXScale(x);
    setYScale(y);
}

void Drawable::setScale(float scale)
{
    setScale(scale, scale);
}

void Drawable::setXScale(float scale)
{
    if (scale >= 0.f)
        m_xScale = scale;
}

float Drawable::getXScale() const
{
    return m_xScale;
}

void Drawable::setYScale(float scale)
{
    if (scale >= 0.f)
        m_yScale = scale;
}

float Drawable::getYScale() const
{
    return m_yScale;
}

    //Rotation
void Drawable::setRotation(float angle)
{
    m_rotationAngle = angle;
}

float Drawable::getRotation() const
{
    return m_rotationAngle;
}

    //Flip
void Drawable::setXFlip(bool flip)
{
    m_xFlip = flip;
}

bool Drawable::getXFlip() const
{
    return m_xFlip;
}

void Drawable::setYFlip(bool flip)
{
    m_yFlip = flip;
}

bool Drawable::getYFlip() const
{
    return m_yFlip;
}
