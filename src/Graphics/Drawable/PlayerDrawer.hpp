/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_DRAWER
#define DEF_PLAYER_DRAWER

#include "Player/StateDescription.hpp"
#include "AnimationDrawer.hpp"
#include "AttackDrawer.hpp"
#include "Polygon.hpp"
#include "Structs/Vector.hpp"
#include <map>

class TextureManager;
class Player;
struct State;

class PlayerDrawer
{
    public:
        PlayerDrawer();
        void attachTo(const Player &player,
                      TextureManager &textureManager, bool showHitboxes);
        void draw();

    private:
        void update();

        /// Utility to draw the moves/animations of a player
        class MovesDrawer : public Drawable
        {
            public:
                MovesDrawer() {}
                void draw() override;
                void update(StateIndex state, AttackIndex attack);
                void load(const std::map<StateIndex, const State*> &stateInfo,
                          TextureManager &textureManager, bool showHitboxes);

            private:
                struct StateDrawer
                {
                    AnimationDrawer animationDraw;
                    std::map<AttackIndex, AttackDrawer> attacksDraw;
                };
                std::map<StateIndex, StateDrawer> m_statesDraw;
                StateIndex m_state; ///< Currently drawn state
                AttackIndex m_attack; ///< Currently drawn attack
        };

        MovesDrawer m_movesDraw;

        Polygon m_shadow; ///< Elliptic shadow under the player
        Vector m_shadowSize; ///< Shadow size

        const Player *m_player;
};

#endif
