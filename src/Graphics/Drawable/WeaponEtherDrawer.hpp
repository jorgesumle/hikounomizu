/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_WEAPON_ETHER_DRAWER
#define DEF_WEAPON_ETHER_DRAWER

#include "Weapon/WeaponEther.hpp"
#include "Sprite.hpp"

class Weapon;
class TextureManager;

class WeaponEtherDrawer : public WeaponEtherCallbackReceiver
{
    public:
        WeaponEtherDrawer();
        void draw();
        void attachTo(WeaponEther &ether, TextureManager &textureManager);

        //WeaponEther callbacks
        void weaponAdded(const Weapon &weapon) override;
        void weaponRemoved(const Weapon &weapon) override;

    private:
        /// Utility to draw a weapon
        class WeaponDrawer : public Drawable
        {
            public:
                WeaponDrawer() : m_weapon(nullptr) {}
                void draw() override;
                void update();
                void attachTo(const Weapon &weapon, Texture &texture);

            private:
                const Weapon *m_weapon;
                Sprite m_sprite;
        };

        std::map<const Weapon*, WeaponDrawer> m_weaponsDraw;
        TextureManager *m_textureManager;
};

#endif
