/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Camera.hpp"

#include "Graphics/GL.hpp"

Camera::Camera(const Box &sceneBox) : m_sceneBox(sceneBox) {}

void Camera::look(float viewWidth, float viewHeight) const
{
    Camera::look(m_sceneBox, viewWidth, viewHeight);
}

void Camera::look(const Box &sceneBox, float viewWidth, float viewHeight)
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glScalef(viewWidth / sceneBox.width, viewHeight / sceneBox.height, 1.f);
    glTranslatef(-sceneBox.left, -sceneBox.top, 0.f);
}

void Camera::look(const Box &sceneBox, const Box &targetBox)
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(targetBox.left, targetBox.top, 0.f);

    glScalef(targetBox.width / sceneBox.width,
             targetBox.height / sceneBox.height, 1.f);
    glTranslatef(-sceneBox.left, -sceneBox.top, 0.f);
}
