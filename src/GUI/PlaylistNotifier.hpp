/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYLIST_NOTIFIER
#define DEF_PLAYLIST_NOTIFIER

#include "Graphics/Drawable/Text.hpp"
#include "Graphics/Drawable/Polygon.hpp"
#include "Tools/Timer.hpp"
#include <mutex>
#include <string>

/// The notify() interface of PlaylistNotifier, that is meant
/// to be called from an external PlaylistPlayer thread
class PlaylistNotifiable
{
    public:
        virtual ~PlaylistNotifiable() = default;
        virtual void notify(const std::string &author,
                            const std::string &title) = 0;
};

/// Drawable used to notify the user that a new song
/// was launched (e.g.: in a playlist)
/// Shows the title of the new song / the author name / ...
/// Shared between the main thread that draws it
/// and a PlaylistPlayer thread that calls notify()
class PlaylistNotifier : public Drawable, public PlaylistNotifiable
{
    public:
        explicit PlaylistNotifier(float viewWidth);
        void draw() override;
        void update();

        /// Called from the thread playing the playlist.
        /// Only sets the string information to be drawed,
        /// the Texts will be set in the main thread.
        /// (A single Freetype's FT_Face can't be called from two threads:
        ///  cf. freetype.org/freetype2/docs/reference/)
        void notify(const std::string &author,
                    const std::string &title) override;

        //Get and set methods
        void setFonts(Font &titleFont, Font &authorFont);

        float getWidth() const;
        float getHeight() const;

    private:
        const float m_viewWidth;

        //Shared data
        std::mutex m_mutex;
        std::string m_author, m_title;
        bool m_updated; ///< Has the data been updated vs. the printed texts?

        //Drawable objects
        Text m_authorText, m_titleText;
        Polygon m_background;

        Timer m_notifyTimer; ///< Timer to limit the notification duration
        bool m_notifying; ///< Is the notification being drawn?
};

#endif
