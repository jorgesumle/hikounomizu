/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_FIGHT_INTERFACE
#define DEF_FIGHT_INTERFACE

#include "PlayerInfo.hpp"
#include <vector>
#include <cstdint>

class FightInterface : public Drawable
{
    public:
        FightInterface(Font &namesFont, Font &weaponsFont, Font &barsFont,
                       float viewWidth);
        void draw() override;
        void update();

        void setPlayers(const std::vector<const Player*> &players,
                        TextureManager &textureManager);
        void replaceNames(const std::vector<Utf8::utf8_string> &playerNames);

        void updatePlayerResults(std::uint8_t roundsToWin,
                                const std::vector<std::uint8_t> &winsPerPlayer);
        void resetPlayerResults();

    private:
        void updatePositions();

        std::vector<PlayerInfo> m_playerInfos;
        Font * const m_namesFont;
        Font * const m_weaponsFont;
        Font * const m_barsFont;

        float m_iconsScale;
        const float m_viewWidth;
};

#endif
