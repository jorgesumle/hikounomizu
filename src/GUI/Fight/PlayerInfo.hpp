/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_INFO
#define DEF_PLAYER_INFO

#include "PlayerResults.hpp"
#include "GUI/ShadowText.hpp"
#include "Graphics/Drawable/Sprite.hpp"
#include "Graphics/Drawable/Polygon.hpp"
#include "Tools/Utf8.hpp"
#include <string>

class Font;
class Player;
class TextureManager;

/// Stores the last known state of the player
/// (health, weapon amount...)
struct KnownState
{
    KnownState() : health(-1), shurikenAmount(0) {}

    int health;
    unsigned int shurikenAmount;
};

/// FightInterface item showing a player's information
class PlayerInfo : public Drawable
{
    public:
        PlayerInfo(const Player &player, TextureManager &textureManager,
                   Font &nameFont, Font &weaponsFont, Font &barsFont,
                   float viewWidth);
        void draw() override;

        void update();

        void replaceName(const Utf8::utf8_string &playerName);
        void setResults(std::uint8_t roundsToWin, std::uint8_t wonRounds = 0);
        void setIconScale(float scale);

        float getWidth() const;
        float getHeight() const;

    private:
        void initPositions();
        void updateWeaponsText();

        const Player * const m_player;

        ShadowText m_name;
        Text m_lifeText;

        Sprite m_icon;
        Polygon m_lifeBar, m_lifeBarBack;

        Sprite m_shurikenIcon;
        ShadowText m_shurikenText;

        PlayerResults m_results;
        KnownState m_knownState;

        const float m_barsHeight, m_barsWidth;
        const float m_viewWidth;
};

#endif
