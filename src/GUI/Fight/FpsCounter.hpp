/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_FPS_COUNTER
#define DEF_FPS_COUNTER

#include "GUI/ShadowText.hpp"
#include "Tools/Timer.hpp"

/// Computes and displays the frames per second value
class FpsCounter : public Drawable
{
    public:
        explicit FpsCounter(Font &font);
        void draw() override;
        void tick(); ///< Counts a rendered frame, to be called once per frame
        void reset(); ///< Reset and restart frame counting

    private:
        ShadowText m_text;

        Timer::TimePoint m_time; ///< Time at which frame counting started
        unsigned int m_ticks; ///< Number of frames counted since m_time

};

/// RAII wrapper to contain an optional FpsCounter object
class OptionalFpsCounter
{
    public:
        /// Constructs a FpsCounter iff \p font is not nullptr
        explicit OptionalFpsCounter(Font *font);
        OptionalFpsCounter(const OptionalFpsCounter &fps) = delete;
        OptionalFpsCounter &operator=(const OptionalFpsCounter &fps) = delete;
        ~OptionalFpsCounter();

        void draw();
        void tick();
        void reset();
        void setPosition(float x, float y);

    private:
        FpsCounter *m_counter;
};

#endif
