/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_ICON
#define DEF_PLAYER_ICON

#include "Graphics/Drawable/Sprite.hpp"
#include "Engines/Resources/TextureManager.hpp"

/// Utility to load character icons based on their name
namespace PlayerIcon
{
    inline Sprite load(const std::string &characterName,
                       TextureManager &textureManager)
    {
        Texture &src = textureManager.getTexture("gfx/characters/reduced.png");

        if (characterName == "Hikou")
            return Sprite(src, Box(0.f, 0.f, .25f, 1.f));
        else if (characterName == "Takino")
            return Sprite(src, Box(.25f, 0.f, .25f, 1.f));
        else if (characterName == "Hana")
            return Sprite(src, Box(.5f, 0.f, .25f, 1.f));

        return Sprite();
    }
}

#endif
