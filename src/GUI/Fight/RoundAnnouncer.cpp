/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "RoundAnnouncer.hpp"
#include "Engines/Sound/SoundEngine.hpp"
#include "Graphics/Resources/Font.hpp"
#include "Fight/FightRules.hpp"
#include "Structs/Vector.hpp"

#include "Tools/ScaledPixel.hpp"
#include "Tools/Format.hpp"
#include "Tools/I18n.hpp" ///< _ macro for i18n

namespace
{
    bool isFinalRound(const FightRules &rules,
                      const std::vector<std::uint8_t> &winsPerPlayer)
    {
        if (winsPerPlayer.size() != rules.getPlayerCount())
            return false;

        const std::uint8_t oneRoundToVictory = rules.getRoundsToWin() - 1;
        for (std::uint8_t winCount : winsPerPlayer)
        {
            if (winCount != oneRoundToVictory)
                return false;
        }

        return true;
    }
}

RoundAnnouncer::RoundAnnouncer(const Vector &viewSize) :
m_soundEngine(nullptr), m_text(ScaledPixel{viewSize.x}(4.f)),
m_duration_ms(0.f), m_viewSize(viewSize)
{

}

void RoundAnnouncer::draw()
{
    if (m_timer.getElapsed() < m_duration_ms)
        m_text.draw();
}

void RoundAnnouncer::clear()
{
    m_duration_ms = 0.f;
}

void RoundAnnouncer::announceRound(unsigned int roundId,
                                const FightRules &rules,
                                const std::vector<std::uint8_t> &winsPerPlayer)
{
    constexpr float readyTime = 1000.f;

    if (rules.getRoundsToWin() <= 1)
        announce(_(RoundAnnouncerReady), readyTime);
    else if (isFinalRound(rules, winsPerPlayer))
        announce(_(RoundAnnouncerFinalRound), readyTime);
    else
        announce(_f(RoundAnnouncerNumberedRound,
                    std::make_pair("round_id", roundId + 1)), readyTime);
}

void RoundAnnouncer::announceFight()
{
    announce(_(RoundAnnouncerFight), 500.f);
    if (m_soundEngine != nullptr)
        m_soundEngine->playSound("audio/ui/fight.ogg", 1.f);
}

void RoundAnnouncer::setSoundEngine(SoundEngine &soundEngine)
{
    m_soundEngine = &soundEngine;
}

void RoundAnnouncer::setSkin(Font &font, const Color &color,
                             const Color &borderColor)
{
    m_text.setFont(font);
    m_text.setColor(color, borderColor);
}

void RoundAnnouncer::announce(const std::string &message, float duration_ms)
{
    m_text.setText(message);
    updatePosition();

    m_duration_ms = duration_ms;
    m_timer.reset();
}

void RoundAnnouncer::updatePosition()
{
    m_text.setPosition((m_viewSize.x - m_text.getWidth()) / 2.f,
                       (m_viewSize.y / 2.f) - m_text.getOriginHeight());
}
