/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_LAYOUT
#define DEF_LAYOUT

/// Tools to adjust layout by computing widget properties like dimensions
namespace Layout
{
    /// Return the element with the greatest width among the variadic arguments
    /// (Tail)
    template <typename...>
    float largest() { return 0.f; }

    /// Return the element with the greatest width among the variadic arguments
    /// (General case)
    template <typename T, typename... Ts>
    float largest(T&& head, Ts&&... tail)
    {
        const float headWidth = head.getWidth();
        const float tailWidth = largest(std::forward<Ts>(tail)...);
        return (headWidth > tailWidth) ? headWidth : tailWidth;
    }
}

#endif
