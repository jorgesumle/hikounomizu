/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Button.hpp"
#include "Engines/Sound/SoundEngine.hpp"

Button::Button(const SkinBox &skin, const SkinSound &sounds) :
Widget(), m_selected(false)
{
    Button::setSkin(skin);
    setSoundSkin(sounds);
}

void Button::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();

    if (m_selected) m_skin.selected.draw();
    else if (m_focused) m_skin.focused.draw();
    else m_skin.inactive.draw();

    Drawable::popMatrix();
}

bool Button::mouseRelease(const Vector &mouseCoords)
{
    if (Widget::contains(Widget::localCoords(mouseCoords)))
    {
        //Launch click sound
        if (!isSelected() && m_sounds.engine != nullptr &&
            !m_sounds.click.empty())
            m_sounds.engine->playSound(m_sounds.click);

        return true;
    }

    return false;
}

bool Button::mouseClick(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

bool Button::mouseMove(const Vector &mouseCoords)
{
    if (Widget::contains(Widget::localCoords(mouseCoords)))
    {
        if (!isSelected() && !isFocused())
        {
            //Launch focus sound
            if (m_sounds.engine != nullptr && !m_sounds.focus.empty())
                m_sounds.engine->playSound(m_sounds.focus);

            setFocus(true);
        }

        return true;
    }

    setFocus(false);
    return false;
}

void Button::select(bool select)
{
    m_selected = select;
}

bool Button::isSelected() const
{
    return m_selected;
}

void Button::setSkin(const SkinBox &skin)
{
    if (skin.box.width < 0.f || skin.box.height < 0.f)
        return;

    m_skin = skin;

    if (m_skin.inactive.getWidth() > 0.f && m_skin.inactive.getHeight() > 0.f)
        m_skin.inactive.setScale(
            m_skin.box.width / m_skin.inactive.getWidth(),
            m_skin.box.height / m_skin.inactive.getHeight());

    if (m_skin.focused.getWidth() > 0.f && m_skin.focused.getHeight() > 0.f)
        m_skin.focused.setScale(
            m_skin.box.width / m_skin.focused.getWidth(),
            m_skin.box.height / m_skin.focused.getHeight());

    if (m_skin.selected.getWidth() > 0.f && m_skin.selected.getHeight() > 0.f)
        m_skin.selected.setScale(
            m_skin.box.width / m_skin.selected.getWidth(),
            m_skin.box.height / m_skin.selected.getHeight());
}

void Button::setSoundSkin(const SkinSound &soundSkin)
{
    m_sounds = soundSkin;
}

float Button::getWidth() const
{
    return m_skin.box.width;
}

float Button::getHeight() const
{
    return m_skin.box.height;
}
