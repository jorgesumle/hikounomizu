/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_KEY_BINDER
#define DEF_KEY_BINDER

#include "Widget.hpp"

#include "Graphics/Drawable/Sprite.hpp"
#include "Graphics/Drawable/Text.hpp"
#include "Tools/Input/Key.hpp"

class SoundEngine;

/// Widget to bind a key:
/// Shows the current binded key and allows to change it using focus
class KeyBinder : public Widget
{
    public:
        KeyBinder();
        KeyBinder(const Key &key, Font &textFont);
        void draw() override;

        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;

        void keyDown(const Key &key);
        void setKey(const Key &key);
        const Key &getKey() const;

        void setFocus(bool focused) override;

        void setInactiveBack(const Sprite &inactive);
        void setFocusedBack(const Sprite &focused);

        void setSoundEngine(SoundEngine &soundEngine);
        void setFocusSound(const std::string &focusSound);
        void setKeySetSound(const std::string &keySetSound);

        float getWidth() const override;
        float getHeight() const override;

    private:
        void centerText();

        Text m_keyText; ///< Key name text
        Sprite m_inactiveBack, m_focusedBack;

        Key m_key; ///< Represented key

        SoundEngine *m_soundEngine; ///< To handle sounds on events
        std::string m_focusSound, m_keySetSound;
};

#endif
