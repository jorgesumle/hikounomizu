/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_TEXT_INPUT_ENABLER
#define DEF_TEXT_INPUT_ENABLER

#include <vector>

class TextInput;
class Viewport;
struct Vector;

/// Utility to enable/disable and position the system text input interface
/// when a TextInput is focused/unfocused.
/// Handles the controlled TextInput's mouse release events, the mouse release
/// callback of individual TextInput does not need to be used directly
class TextInputEnabler
{
    public:
        TextInputEnabler(const std::vector<TextInput*> &inputs,
                         const Viewport &viewport);
        void mouseRelease(const Vector &mouseCoords);

    private:
        const std::vector<TextInput*> m_textInputs;
        const Viewport &m_viewport;

        /// The currently focused text input, or nullptr if none
        const TextInput *m_currentInput;
};

#endif
