/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ShadowText.hpp"

ShadowText::ShadowText(float shadowSize) : Drawable()
{
    m_shadows[0].setPosition(-shadowSize, -shadowSize);
    m_shadows[1].setPosition(-shadowSize, shadowSize);
    m_shadows[2].setPosition(shadowSize, -shadowSize);
    m_shadows[3].setPosition(shadowSize, shadowSize);
}

void ShadowText::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();

    for (Text &shadow : m_shadows)
        shadow.draw();

    m_text.draw();

    Drawable::popMatrix();
}

void ShadowText::setColor(const Color &nameColor, const Color &shadowColor)
{
    m_text.setColor(nameColor);

    for (Text &shadow : m_shadows)
        shadow.setColor(shadowColor);
}

void ShadowText::setText(const Utf8::utf8_string &text)
{
    m_text.setText(text);

    for (Text &shadow : m_shadows)
        shadow.setText(text);
}

void ShadowText::setFont(Font &font)
{
    m_text.setFont(font);

    for (Text &shadow : m_shadows)
        shadow.setFont(font);
}

const Font *ShadowText::getFont() const
{
    return m_text.getFont();
}

float ShadowText::getWidth() const
{
    return m_text.getWidth();
}

float ShadowText::getHeight() const
{
    return m_text.getHeight();
}

float ShadowText::getOriginHeight() const
{
    return m_text.getOriginHeight();
}
