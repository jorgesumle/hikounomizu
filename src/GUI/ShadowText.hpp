/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SHADOW_TEXT
#define DEF_SHADOW_TEXT

#include "Graphics/Drawable/Text.hpp"
#include <array>

/// ShadowText: Text with shadow border effects
/// to print, e.g., character names, usernames
class ShadowText : public Drawable
{
    public:
        explicit ShadowText(float shadowSize = 1.f);
        void draw() override;

        void setColor(const Color &nameColor, const Color &shadowColor);
        void setText(const Utf8::utf8_string &text);

        void setFont(Font &font);
        const Font *getFont() const;

        float getWidth() const;
        float getHeight() const;
        float getOriginHeight() const;

    private:
        Text m_text;
        std::array<Text, 4> m_shadows;
};

#endif
