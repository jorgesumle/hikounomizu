/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_WEAPON_ETHER
#define DEF_WEAPON_ETHER

#include "WeaponType.hpp"
#include <map>
#include <set>

class Weapon;
class Player;
class PhysicsWorld;
class SoundInterface;
class EtherStatus;
struct Vector;

class WeaponEtherCallbackReceiver
{
    public:
        virtual ~WeaponEtherCallbackReceiver() {}
        virtual void weaponAdded(const Weapon &weapon) = 0;
        virtual void weaponRemoved(const Weapon &weapon) = 0;
};

/// Medium keeping track of weapons in game.
/// Allows to throw and delete weapons properly.
class WeaponEther
{
    public:
        WeaponEther();
        WeaponEther(const WeaponEther &copied) = delete;
        WeaponEther &operator=(const WeaponEther &copied) = delete;
        ~WeaponEther();

        void setSoundInterface(SoundInterface *soundInterface);
        void setCallbackReceiver(WeaponEtherCallbackReceiver *callbackReceiver);

        void update(float frameTime);
        void queueForRemoval(unsigned int weaponID);
        void clear(); ///< Remove all known weapons immediately

        bool throwShuriken(PhysicsWorld &world, const Player &player);

        void applyStatus(PhysicsWorld &world, const EtherStatus &status);
        EtherStatus getStatus() const;

    private:
        void processQueue(); ///< Remove weapons which were queued for removal
        bool throwWeapon(PhysicsWorld &world,
                         WeaponType weaponType, unsigned int weaponIx,
                         const Vector &position, const Vector &velocity);

        std::map<unsigned int, Weapon*> m_weaponsList;
        std::set<unsigned int> m_toBeRemoved; ///< Weapons to be removed
        unsigned int m_weaponIx; ///< Always a free weapon index

        WeaponEtherCallbackReceiver *m_callbackReceiver;
        SoundInterface *m_soundInterface;
};

#endif
