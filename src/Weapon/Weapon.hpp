/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_WEAPON
#define DEF_WEAPON

#include "Engines/Physics/PhysicsObject.hpp"

class WeaponEther;
class SoundInterface;
class WeaponStatus;

/// PhysicsObject's inheriting class representing a throwable weapon
class Weapon : public PhysicsObject
{
    public:
        Weapon(WeaponEther &ether, unsigned int id /**< The ether is responsible
                                    for providing a valid and unique ID */);

        /// Tell the ether that this weapon is ready to be removed next frame
        void queueForRemoval();

        /// Remove the weapon from the physics world
        void removeFromWorld();

        void setSoundInterface(SoundInterface *soundInterface);

        void collideWorld(bool ground) override;
        void collide(PhysicsObject &obj) override;
        void pushDamage(float strength, HitDirection side,
                        const Player &source,
                        DamageTracker &/*damage*/) override;

        virtual void update(float frameTime) = 0;

        virtual void applyStatus(const WeaponStatus &status) = 0;
        virtual const WeaponStatus &getStatus() const = 0;

        virtual float getAngle() const = 0;
        virtual const std::string &getTexturePath() const = 0;
        virtual const Box &getSourceBox() const = 0;

    protected:
        WeaponEther *m_ether;
        unsigned int m_inEtherID;

        SoundInterface *m_soundInterface;
        bool m_disabled;
};

#endif
