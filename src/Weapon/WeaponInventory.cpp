/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "WeaponInventory.hpp"
#include "WeaponEther.hpp"
#include "Player/Player.hpp"
#include "Engines/Physics/PhysicsWorld.hpp"
#include "Network/Serialization/InventoryStatus.hpp"

WeaponInventory::WeaponInventory() : m_player(nullptr), m_ether(nullptr) {}

WeaponInventory::WeaponInventory(const Player &player, WeaponEther &ether) :
m_player(&player), m_ether(&ether)
{
    reset();
}

void WeaponInventory::throwShuriken(PhysicsWorld &world)
{
    if (m_ether != nullptr && m_player != nullptr &&
        m_slots[WeaponType::Shuriken] > 0 &&
        m_ether->throwShuriken(world, (*m_player)))
    {
        m_slots[WeaponType::Shuriken]--;
    }
}

unsigned int WeaponInventory::getRemaining(WeaponType weaponType) const
{
    try
    {
        return m_slots.at(weaponType);
    } catch (const std::exception &) {}

    return 0;
}

void WeaponInventory::reset()
{
    m_slots[WeaponType::Shuriken] = 5;
}

void WeaponInventory::applyStatus(const InventoryStatus &status)
{
    m_slots[WeaponType::Shuriken] = status.getRemaining(WeaponType::Shuriken);
}
