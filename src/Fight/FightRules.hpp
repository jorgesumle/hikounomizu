/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_FIGHT_OPTIONS
#define DEF_FIGHT_OPTIONS

#include <cstdint>

/// Rules describing a game/fight, such as the mode,
/// number of rounds, players...
class FightRules
{
    public:
        enum class Mode : std::uint8_t
        {
            Default = 0 /**< Lets each player move and look around
                             independently, useful especially for games
                             with more than 2 players */,

            Versus = 1 /**< Two players only, locks the looking direction
                            of each player on the other */
        };

        FightRules();

        void setMode(Mode mode);
        Mode getMode() const;

        void setRoundsToWin(std::uint8_t rounds);
        std::uint8_t getRoundsToWin() const;

        void setPlayerCount(std::uint8_t players);
        std::uint8_t getPlayerCount() const;

    private:
        Mode m_mode;
        std::uint8_t m_roundsToWin;
        std::uint8_t m_playerCount;
};

#endif
