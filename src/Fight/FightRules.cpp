/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FightRules.hpp"

FightRules::FightRules() :
m_mode(FightRules::Mode::Default), m_roundsToWin(2), m_playerCount(2)
{

}

void FightRules::setMode(FightRules::Mode mode)
{
    m_mode = mode;
    if (m_mode == FightRules::Mode::Versus && m_playerCount != 2)
        m_playerCount = 2;
}

FightRules::Mode FightRules::getMode() const
{
    return m_mode;
}

void FightRules::setRoundsToWin(std::uint8_t rounds)
{
    if (rounds > 0)
        m_roundsToWin = rounds;
}

std::uint8_t FightRules::getRoundsToWin() const
{
    return m_roundsToWin;
}

void FightRules::setPlayerCount(std::uint8_t players)
{
    if (m_mode == Mode::Default && players > 1)
        m_playerCount = players;
}

std::uint8_t FightRules::getPlayerCount() const
{
    return m_playerCount;
}
