/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_VERSUS_CONTROLLER
#define DEF_VERSUS_CONTROLLER

#include "Fight/FightRules.hpp"
#include <vector>

class Player;

namespace FightModeControl
{
    class Controller
    {
        public:
            virtual ~Controller() = default;
            virtual void preUpdate(std::vector<Player*> &players) const = 0;
            virtual void postUpdate(std::vector<Player*> &players) const = 0;
    };

    /// Allocates a fight mode controller matching the input \p mode
    /// May not allocate and return nullptr if a specific mode requires
    /// no controller (such as FightRules::Mode::Default)
    Controller *allocate(FightRules::Mode mode);
}


class VersusController : public FightModeControl::Controller
{
    public:
        void preUpdate(std::vector<Player*> &players) const override;
        void postUpdate(std::vector<Player*> &players) const override;
};

#endif
