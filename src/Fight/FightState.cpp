/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FightState.hpp"
#include "Player/Player.hpp"

FightState::FightState(std::size_t playerCount) :
m_phase(Phase::Ready), m_roundId(0), m_winsPerPlayer(playerCount, 0)
{

}

FightState::FightState(FightState::Phase phase, unsigned int roundId,
                       const RoundResult &lastResult,
                       const std::vector<std::uint8_t> &winsPerPlayer) :
m_phase(phase), m_roundId(roundId),
m_lastResult((lastResult.drawGame || lastResult.winnerIx < winsPerPlayer.size())
             ? lastResult : RoundResult()), //Only allow valid winner indexes
m_winsPerPlayer(winsPerPlayer)
{

}

void FightState::setToReady()
{
    m_phase = Phase::Ready;
}

void FightState::setToFighting()
{
    m_phase = Phase::Fighting;
}

void FightState::finishDraw()
{
    //Prepare next round identifier
    m_roundId++;

    m_lastResult = RoundResult();
    m_phase = Phase::Break;
}

std::uint8_t FightState::finishWinner(std::size_t winnerIx,
                                      std::uint8_t roundsToWin)
{
    if (winnerIx >= m_winsPerPlayer.size())
        return 0;

    //Prepare next round identifier
    m_roundId++;

    if (m_winsPerPlayer[winnerIx] < roundsToWin)
        m_winsPerPlayer[winnerIx]++;

    m_lastResult = RoundResult(winnerIx);
    m_phase = (m_winsPerPlayer[winnerIx] == roundsToWin) ? Phase::Finished
                                                         : Phase::Break;
    return m_winsPerPlayer[winnerIx];
}

bool FightState::playerWasErased(std::size_t playerIx)
{
    if (playerIx >= m_winsPerPlayer.size())
        return false;

    //Remove the wins history of the player
    m_winsPerPlayer.erase(std::next(m_winsPerPlayer.begin(),
        static_cast<std::vector<std::uint8_t>::difference_type>(playerIx)));

    //If the last game was a draw, nothing to do
    if (!m_lastResult.drawGame)
    {
        if (m_lastResult.winnerIx == playerIx)
            //The erased player had won the last round, make it a draw game
            m_lastResult = RoundResult();
        else if (m_lastResult.winnerIx > playerIx)
            //The last winner's player ix has changed following the removal
            m_lastResult.winnerIx--;
    }

    //Not enough players to continue, finish immediately
    if (m_winsPerPlayer.size() < 2)
    {
        m_phase = Phase::Finished;

        //If only one player is left and won at least one round,
        //give them the win
        if (!m_winsPerPlayer.empty() && m_winsPerPlayer[0] > 0)
            m_lastResult = RoundResult(0);
    }

    return true;
}

FightState::Phase FightState::getPhase() const
{
    return m_phase;
}

const RoundResult &FightState::getLastRoundResult() const
{
    return m_lastResult;
}

const std::vector<std::uint8_t> &FightState::getWinsPerPlayer() const
{
    return m_winsPerPlayer;
}

unsigned int FightState::getRoundId() const
{
    return m_roundId;
}
