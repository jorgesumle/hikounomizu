/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_JOYSTICK_DATA
#define DEF_JOYSTICK_DATA

#include "Player/PlayerKeys.hpp"
#include <string>

#define JOYSTICK_INSTANCE_INVALID -1

struct JoystickData
{
    JoystickData() : instanceID(JOYSTICK_INSTANCE_INVALID) {}
    JoystickData(int joyInstance,
                 const std::string &joyName,
                 const PlayerKeys &joyKeys) :
                 instanceID(joyInstance), name(joyName), keys(joyKeys) {}

    int instanceID;
    std::string name;
    PlayerKeys keys;
};

#endif
