/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_VECTOR
#define DEF_VECTOR

#include <cmath>

struct Vector
{
    Vector() : x(0.f), y(0.f) {}

    Vector(float xCoords, float yCoords) : x(xCoords), y(yCoords)
    {

    }

    Vector operator*(float scale) const
    {
        return Vector(scale * x, scale * y);
    }

    Vector operator+(const Vector &vect) const
    {
        return Vector(x + vect.x, y + vect.y);
    }

    Vector operator-(const Vector &vect) const
    {
        return Vector(x - vect.x, y - vect.y);
    }

    float getAngle() const
    {
        return std::atan2(y,x);
    }

    float getNorm() const
    {
        return std::sqrt(x*x + y*y);
    }

    float x, y;
};

#endif
