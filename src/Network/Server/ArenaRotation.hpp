/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ARENA_ROTATION
#define DEF_ARENA_ROTATION

#include <random>
#include <vector>
#include <string>
#include <cstddef>

/// Wrapper to hold the current server arena, if initialized
struct ArenaIndex
{
    ArenaIndex() : initialized(false), index(0) {}

    bool initialized;
    std::size_t index;
};

/// Holds the currently playing arena on the server
/// and allows to select the next server arena
class ArenaRotation
{
    public:
        explicit ArenaRotation(const std::vector<std::string> &arenas);

        /// Choose and return the name of a new server arena
        std::string choose();
        std::string getArena() const; ///< Get the current server arena

    private:
        std::mt19937 m_generator;
        std::vector<std::string> m_arenas;
        ArenaIndex m_ix;
};

#endif
