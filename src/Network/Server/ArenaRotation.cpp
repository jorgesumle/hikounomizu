/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ArenaRotation.hpp"

#include "Tools/Log.hpp"

ArenaRotation::ArenaRotation(const std::vector<std::string> &arenas) :
m_generator(std::random_device{}()), m_arenas(arenas)
{
    choose();
}

std::string ArenaRotation::choose()
{
    if (m_arenas.empty())
    {
        m_ix.initialized = false;
        return "";
    }

    std::uniform_int_distribution<std::size_t> dist(0, m_arenas.size() - 1);
    std::size_t arenaIx = dist(m_generator);
    if (m_ix.initialized && m_arenas.size() >= 2)
    {
        while (arenaIx == m_ix.index)
            arenaIx = dist(m_generator);
    }

    m_ix.index = arenaIx;
    m_ix.initialized = true;

    return m_arenas[m_ix.index];
}

std::string ArenaRotation::getArena() const
{
    return (m_ix.initialized && m_ix.index < m_arenas.size()) ?
            m_arenas[m_ix.index] : "";
}
