/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_KICK_REASON
#define DEF_KICK_REASON

#include "Tools/I18n.hpp" ///< _ macro for i18n
#include <string>
#include <cstdint>

/// Encodes the reasons describing why a peer was disconnected
enum class KickReason : std::uint32_t
{
    ConnectionLost = 0,
    VersionMismatch = 1,
    ServerIsFull = 2,
    ServerNoCharacter = 3,
    Inactivity = 4,
    IllegalCommand = 5,
    UnintelligibleByServer = 6,
    UnintelligibleByClient = 7,
    ConnectionTimeout = 8,
    ClientNoArena = 9,
    ClientNoCharacter = 10,
    ClientDecision = 11,
    EnetError = 12,
    Unknown = 13
};

namespace KickInfo
{
    inline KickReason getReason(std::uint32_t reasonCode)
    {
        if (reasonCode < static_cast<std::uint32_t>(KickReason::Unknown))
            return static_cast<KickReason>(reasonCode);

        return KickReason::Unknown;
    }

    inline std::string describe(KickReason reason)
    {
        if (reason == KickReason::ConnectionLost)
            return _(KickedConnectionLost);
        else if (reason == KickReason::VersionMismatch)
            return _(KickedVersionMismatch);
        else if (reason == KickReason::ServerIsFull)
            return _(KickedServerIsFull);
        else if (reason == KickReason::ServerNoCharacter)
            return _(KickedServerNoCharacter);
        else if (reason == KickReason::Inactivity)
            return _(KickedInactivity);
        else if (reason == KickReason::IllegalCommand)
            return _(KickedIllegalCommand);
        else if (reason == KickReason::UnintelligibleByServer)
            return _(KickedUnintelligibleByServer);
        else if (reason == KickReason::UnintelligibleByClient)
            return _(KickedUnintelligibleByClient);
        else if (reason == KickReason::ConnectionTimeout)
            return _(KickedConnectionTimeout);
        else if (reason == KickReason::ClientNoArena)
            return _(KickedClientNoArena);
        else if (reason == KickReason::ClientNoCharacter)
            return _(KickedClientNoCharacter);
        else if (reason == KickReason::ClientDecision)
            return _(KickedClientDecision);
        else if (reason == KickReason::EnetError)
            return _(KickedENetError);

        return _(KickedUnknownReason);
    }
}

#endif
