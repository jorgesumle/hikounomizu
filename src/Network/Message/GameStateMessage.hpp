/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_GAME_STATE_MESSAGE
#define DEF_GAME_STATE_MESSAGE

#include "Message.hpp"
#include "Network/Serialization/PlayerStatus.hpp"
#include "Network/Serialization/PhysicsObjectStatus.hpp"
#include "Network/Serialization/DamageStatus.hpp"
#include "Network/Serialization/EtherStatus.hpp"
#include "Tools/Timer.hpp"
#include <vector>

class GameMetadata;
struct PeerRole;

/// Contains the current game state, sent from server to clients very regularly
/// Optionally contains the game metadata if it has been updated
class GameStateMessage : public Message
{
    public:
        GameStateMessage();
        GameStateMessage(const Timer::duration_uint64_us &clockTicks,
                         const std::vector<PlayerStatus> &playerStatus,
                         const std::vector<PhysicsObjectStatus> &platformStatus,
                         const std::vector<DamageStatus> &damageStatus,
                         const EtherStatus &etherStatus);
        GameStateMessage(const GameStateMessage &copied) = delete;
        GameStateMessage &operator=(const GameStateMessage &copied) = delete;
        ~GameStateMessage();

        /// Set game metadata information to the message
        /// (useful in case of an update)
        void setGameMetadata(const GameMetadata &metadata);

        /// Set the peer role of the game metadata if has been initialized
        void setMetadataPeerRole(const PeerRole &role);

        bool fromBuffer(const Message::Buffer &buffer) override;

        const Timer::duration_uint64_us &getClockTicks() const;
        const GameMetadata *getGameMetadata() const;
        const std::vector<PlayerStatus> &getPlayerStatus() const;
        const std::vector<PhysicsObjectStatus> &getPlatformStatus() const;
        const std::vector<DamageStatus> &getDamageStatus() const;
        const EtherStatus &getEtherStatus() const;

        Message::Buffer allocBuffer() const override;

    private:
        /// Server clock tick that corresponds to the fight status
        Timer::duration_uint64_us m_ticks;

        GameMetadata *m_metadata; ///< Optional updated game metadata
        std::vector<PlayerStatus> m_players;
        std::vector<PhysicsObjectStatus> m_platforms;
        std::vector<DamageStatus> m_damages;
        EtherStatus m_weaponEther;
};

#endif
