/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PEER_PING_MESSAGE
#define DEF_PEER_PING_MESSAGE

#include "Message.hpp"
#include <cstdint>
#include <vector>
#include <list>

/// Serializes a ping value to a 16-bit unsigned integer
class PingSerializer
{
    public:
        PingSerializer();
        explicit PingSerializer(std::uint32_t ping);

        bool readFromBuffer(std::list<std::uint8_t> &bytes);
        bool writeToBuffer(std::list<std::uint8_t> &dst) const;

        std::uint32_t getPing() const;

    private:
        std::uint32_t m_ping;
};

/// A message to send ping information from the server to the peers
class PeerPingMessage : public Message
{
    public:
        PeerPingMessage() = default;
        explicit PeerPingMessage(const std::vector<std::uint32_t> &pings);

        bool fromBuffer(const Message::Buffer &buffer) override;
        Message::Buffer allocBuffer() const override;

        std::vector<std::uint32_t> getPings() const;

    private:
        std::vector<PingSerializer> m_pings;
};

#endif
