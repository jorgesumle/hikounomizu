/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ServerEventMessage.hpp"

ServerEventMessage::ServerEventMessage(const WelcomeEvent &welcome) :
m_event(welcome)
{

}

ServerEventMessage::ServerEventMessage(const RulesEvent &rules) :
m_event(rules)
{

}

ServerEventMessage::ServerEventMessage(const PeerConnectedEvent &peerConnected) :
m_event(peerConnected)
{

}

ServerEventMessage::ServerEventMessage(const PeerDisconnectedEvent &peerDisconnected) :
m_event(peerDisconnected)
{

}

ServerEventMessage::ServerEventMessage(const PeerInactiveEvent &peerInactive) :
m_event(peerInactive)
{

}

ServerEventMessage::ServerEventMessage(const MatchmakingEvent &matchmaking) :
m_event(matchmaking)
{

}

ServerEventMessage::ServerEventMessage(const GameScheduledEvent &gameScheduled) :
m_event(gameScheduled)
{

}

bool ServerEventMessage::fromBuffer(const Message::Buffer &buffer)
{
    std::list<std::uint8_t> bytes = buffer.bytes();
    if (bytes.empty())
        return false;

    return m_event.readFromBuffer(bytes);
}

Message::Buffer ServerEventMessage::allocBuffer() const
{
    std::list<std::uint8_t> bytes;

    if (!m_event.writeToBuffer(bytes))
        return Message::Buffer();

    return Message::Buffer::alloc(bytes);
}

const ServerEvent &ServerEventMessage::getServerEvent() const
{
    return m_event;
}
