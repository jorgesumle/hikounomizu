/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CLIENT_HANDSHAKE_MESSAGE
#define DEF_CLIENT_HANDSHAKE_MESSAGE

#include "Network/Serialization/PeerData.hpp"
#include "Network/Serialization/Version.hpp"
#include "Message.hpp"

class ClientHandshakeMessage : public Message
{
    public:
        ClientHandshakeMessage() = default;
        explicit ClientHandshakeMessage(const PeerData &peerData);

        bool fromBuffer(const Message::Buffer &buffer) override;
        Message::Buffer allocBuffer() const override;

        const Version &getClientVersion() const;
        const PeerData &getPeerData() const;

    private:
        Version m_clientVersion;
        PeerData m_peerData;
};

#endif
