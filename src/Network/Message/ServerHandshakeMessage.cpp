/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ServerHandshakeMessage.hpp"

#include "Tools/ByteTools.hpp"

bool ServerHandshakeMessage::fromBuffer(const Message::Buffer &buffer)
{
    std::list<std::uint8_t> bytes = buffer.bytes();
    return m_serverVersion.readFromBuffer(bytes);
}

Message::Buffer ServerHandshakeMessage::allocBuffer() const
{
    std::list<std::uint8_t> bytes;
    m_serverVersion.writeToBuffer(bytes);

    return Message::Buffer::alloc(bytes);
}

const Version &ServerHandshakeMessage::getServerVersion() const
{
    return m_serverVersion;
}
