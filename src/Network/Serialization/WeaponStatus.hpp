/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_WEAPON_STATUS
#define DEF_WEAPON_STATUS

#include "PhysicsObjectStatus.hpp"
#include "Weapon/WeaponType.hpp"
#include "Structs/Vector.hpp"
#include <cstdint>
#include <list>

class Weapon;

/// Describes the current status of a weapon,
/// used in networked games to exchange weapon information.
class WeaponStatus : public PhysicsObjectStatus
{
    public:
        WeaponStatus();

        /// Read from a weapon
        void readFromWeapon(WeaponType type, unsigned int id,
                            const Weapon &weapon);

        /// Apply the position and velocity to a weapon
        void applyToWeapon(Weapon &weapon) const;

        /// Read/write from buffer
        bool readFromBuffer(std::list<std::uint8_t> &bytes) override;
        bool writeToBuffer(std::list<std::uint8_t> &dst) const override;

        WeaponType getType() const;
        unsigned int getIdentifier() const;

    private:
        WeaponType m_weaponType;
        unsigned int m_weaponId; ///< Identifier of the weapon in the ether
};

#endif
