/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "InventoryStatus.hpp"
#include "Player/Player.hpp"

#include "Tools/Log.hpp"

InventoryStatus::InventoryStatus() : m_shurikens(0)
{

}

void InventoryStatus::readFromPlayer(const Player &player)
{
    m_shurikens = player.getWeaponsRemaining(WeaponType::Shuriken);
}

bool InventoryStatus::readFromBuffer(std::list<std::uint8_t> &bytes)
{
    if (bytes.empty())
    {
        Log::err("Cannot read weapon inventory (empty)");
        return false;
    }

    m_shurikens = static_cast<unsigned int>(bytes.front());
    bytes.pop_front();
    return true;
}

bool InventoryStatus::writeToBuffer(std::list<std::uint8_t> &dst) const
{
    if (m_shurikens >= 256)
    {
        Log::err("Cannot write weapon inventory (overflow)");
        return false;
    }

    dst.push_back(static_cast<std::uint8_t>(m_shurikens));
    return true;
}

unsigned int InventoryStatus::getRemaining(WeaponType weaponType) const
{
    if (weaponType == WeaponType::Shuriken)
        return m_shurikens;

    return 0;
}
