/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PlayerAction.hpp"
#include "Player/Player.hpp"

#include "Tools/Log.hpp"

PlayerAction::PlayerAction() : PlayerAction(PlayerAction::Index::Jump) {}

PlayerAction::PlayerAction(Index actionIndex) : m_actionIndex(actionIndex)
{

}

void PlayerAction::applyPunctual(Player &player) const
{
    if (m_actionIndex == PlayerAction::Index::Jump)
        player.jump();
    else if (m_actionIndex == PlayerAction::Index::Punch)
        player.punch();
    else if (m_actionIndex == PlayerAction::Index::Kick)
        player.kick();
    else if (m_actionIndex == PlayerAction::Index::ThrowShuriken)
        player.throwShuriken();
}

bool PlayerAction::readFromBuffer(std::list<std::uint8_t> &bytes)
{
    if (bytes.empty() || bytes.front() > PlayerAction::IndexLimit)
    {
        Log::err("Invalid or unknown player action received");
        return false;
    }

    m_actionIndex = static_cast<PlayerAction::Index>(bytes.front());

    bytes.pop_front();
    return true;
}

bool PlayerAction::writeToBuffer(std::list<std::uint8_t> &dst) const
{
    dst.push_back(static_cast<std::uint8_t>(m_actionIndex));
    return true;
}

PlayerAction::Index PlayerAction::getIndex() const
{
    return m_actionIndex;
}
