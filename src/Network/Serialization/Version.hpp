/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_VERSION
#define DEF_VERSION

#include "Tools/ByteTools.hpp"
#include "Tools/BuildValues.hpp" ///< Generated at build time

class Version
{
    public:
        Version() : m_version(BuildValues::VERSION) {}

        explicit Version(const BuildValues::Version &version) :
        m_version(version) {}

        //Read/write from buffer
        bool readFromBuffer(std::list<std::uint8_t> &bytes)
        {
            if (bytes.size() < 3)
                return false;

            m_version.major = bytes.front();
            bytes.pop_front();
            m_version.minor = bytes.front();
            bytes.pop_front();
            m_version.patch = bytes.front();
            bytes.pop_front();

            return true;
        }

        void writeToBuffer(std::list<std::uint8_t> &dst) const
        {
            dst.push_back(m_version.major);
            dst.push_back(m_version.minor);
            dst.push_back(m_version.patch);
        }

        bool compatibleWith(const BuildValues::Version &version) const
        {
            //By convention, the server and client versions are considered
            //compatible as long as they share the same major and minor versions
            //The patch number may vary
            return (m_version.major == version.major &&
                    m_version.minor == version.minor);
        }

        std::string text() const
        {
            return m_version.text();
        }

    private:
        BuildValues::Version m_version;
};

#endif
