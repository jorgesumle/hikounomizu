/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_MATCHMAKING_STRATEGY
#define DEF_MATCHMAKING_STRATEGY

#include <list>
#include <string>
#include <cstdint>

namespace MatchmakingStrategy
{
    enum class Type : std::uint8_t
    {
        Stack = 0,
        Random = 1,
        Challenger = 2
    };

    //Read/write from buffers
    void writeStrategy(const Type &strategy, std::list<std::uint8_t> &dst);
    bool readStrategy(std::list<std::uint8_t> &bytes, Type &dst);

    //Read/write from string values
    bool isValid(const std::string &strategyValue);
    std::string getValue(Type strategy);
    Type getStrategy(const std::string &strategyValue);
}

#endif
