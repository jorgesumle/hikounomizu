/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PEER_DATA
#define DEF_PEER_DATA

#include "Tools/Utf8.hpp"
#include "Tools/ByteTools.hpp"
#include "Structs/Character.hpp"
#include <string>
#include <vector>

struct PeerRole
{
    PeerRole() : spectator(true), playerIx(0) {}

    explicit PeerRole(std::size_t player) :
    spectator(false), playerIx(player) {}

    bool operator==(const PeerRole &other) const
    {
        return (spectator && other.spectator) ||
            (!spectator && !other.spectator && playerIx == other.playerIx);
    }

    /// Return whether this peer role controls the player of index \p ix
    bool hasControlOver(std::size_t ix) const
    {
        return (!spectator && playerIx == ix);
    }

    //Read/write from buffer
    bool readFromBuffer(std::list<std::uint8_t> &bytes)
    {
        if (bytes.empty())
            return false;

        spectator = (bytes.front() == 0);
        bytes.pop_front();

        if (!spectator)
        {
            if (bytes.empty())
                return false;

            playerIx = bytes.front();
            bytes.pop_front();
        }

        return true;
    }

    bool writeToBuffer(std::list<std::uint8_t> &dst) const
    {
        if (!spectator && playerIx > 255)
            return false;

        dst.push_back(spectator ? 0 : 255);
        if (!spectator)
            dst.push_back(static_cast<std::uint8_t>(playerIx));

        return true;
    }

    bool spectator; ///< Whether the peer is a spectator
    ///< If the peer is not a spectator, the index of the player it controls
    std::size_t playerIx;
};

struct PeerData
{
    enum : size_t { MaxUsernameLength = 24 };

    PeerData() = default;
    PeerData(const Utf8::utf8_string &user, const std::string &characterName) :
    username(user), character(characterName) {}

    bool operator==(const PeerData &other) const
    {
        return username == other.username && character == other.character;
    }

    bool operator!=(const PeerData &other) const
    {
        return !((*this) == other);
    }

    //Read/write from buffer
    bool readFromBuffer(std::list<std::uint8_t> &bytes)
    {
        return (ByteTools::readUtf8(bytes, username) &&
                ByteTools::readString(bytes, character) &&
                username.length() <= PeerData::MaxUsernameLength);
    }

    bool writeToBuffer(std::list<std::uint8_t> &dst) const
    {
        return (username.length() <= PeerData::MaxUsernameLength &&
                ByteTools::writeUtf8(username, dst) &&
                ByteTools::writeString(character, dst));
    }

    static bool validate(const std::vector<PeerData> &peers,
                         std::vector<std::string> supportedCharacters)
    {
        for (const PeerData &peerData : peers)
        {
            if (!Character::validateName(supportedCharacters,
                                         peerData.character))
                return false;
        }

        return true;
    }

    Utf8::utf8_string username;
    std::string character;
};

#endif
