/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PhysicsObjectStatus.hpp"
#include "Engines/Physics/PhysicsObject.hpp"

#include "Tools/ByteTools.hpp"

void PhysicsObjectStatus::readFromObject(const PhysicsObject &object)
{
    //Position
    const Box &box = object.getBox();
    m_position.x = box.left;
    m_position.y = box.top;

    //Velocity
    m_velocity = object.getVelocity();
}

bool PhysicsObjectStatus::readFromBuffer(std::list<std::uint8_t> &bytes)
{
    return (ByteTools::readVector(bytes, m_position) &&
            ByteTools::readVector(bytes, m_velocity));
}

bool PhysicsObjectStatus::writeToBuffer(std::list<std::uint8_t> &dst) const
{
    ByteTools::writeVector(m_position, dst);
    ByteTools::writeVector(m_velocity, dst);
    return true;
}

const Vector &PhysicsObjectStatus::getPosition() const
{
    return m_position;
}

const Vector &PhysicsObjectStatus::getVelocity() const
{
    return m_velocity;
}
