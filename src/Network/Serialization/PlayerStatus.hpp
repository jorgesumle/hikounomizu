/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_STATUS
#define DEF_PLAYER_STATUS

#include "InventoryStatus.hpp"
#include "PhysicsObjectStatus.hpp"
#include "Player/StateDescription.hpp"
#include "Structs/Vector.hpp"
#include <cstdint>
#include <list>

class Player;

/// Describes the current status of a player,
/// used in networked games to exchange player information
class PlayerStatus : public PhysicsObjectStatus
{
    public:
        PlayerStatus();

        /// Read from Player
        void readFromPlayer(const Player &player);

        /// Read/write from buffer
        bool readFromBuffer(std::list<std::uint8_t> &bytes) override;
        bool writeToBuffer(std::list<std::uint8_t> &dst) const override;

        /// Access status
        float getHealth() const;
        StateIndex getState() const;
        AttackIndex getAttack() const;
        MovingState getMovingState() const;
        float getAnimationTime() const;
        bool getLooksLeft() const;
        const Vector &getSize() const;
        const InventoryStatus &getInventoryStatus() const;

    private:
        float m_health;
        StateIndex m_state;
        AttackIndex m_attack;
        MovingState m_movingState;
        float m_animationTime;
        bool m_lookingLeft;
        Vector m_size;

        InventoryStatus m_inventoryStatus;
};

#endif
