/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CHARACTER_CHOOSER
#define DEF_CHARACTER_CHOOSER

#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/TextButton.hpp"

class CharacterSelectionCallback;

class CharacterChooser : public Tab
{
    public:
        CharacterChooser(Font &textFont, const SkinBox &skin,
                         const SkinSound &sounds, float spacing);
        void draw() override;

        void initialize(CharacterSelectionCallback &callback);

        //Widget methods
        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;

    private:
        TextButton m_hikou, m_hikouAI;
        TextButton m_takino, m_takinoAI;
        TextButton m_hana, m_hanaAI;

        /// Callback that will be used for preview of characters
        CharacterSelectionCallback *m_callback;
};

#endif
