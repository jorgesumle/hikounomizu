/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ARENA_PREVIEW_CALLBACK
#define DEF_ARENA_PREVIEW_CALLBACK

#include <cstddef>

class ArenaPreviewCallback
{
    public:
        virtual ~ArenaPreviewCallback() {}
        virtual void previewChanged(std::size_t arenaIx) = 0;
};

#endif
