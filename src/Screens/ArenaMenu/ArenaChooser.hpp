/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ARENA_CHOOSER
#define DEF_ARENA_CHOOSER

#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/TextButton.hpp"
#include <vector>
#include <string>

class TextureManager;
class ArenaPreviewCallback;

/// The graphical menu used to preview
/// (through an external ArenaPreviewCallback) and choose arenas
class ArenaChooser : public Tab
{
    public:
        ArenaChooser(Font &textFont, TextureManager &manager,
                     const SkinSound &sounds, std::size_t rows,
                     float rowWidth, float rowHeight, float spacing,
                     const Color &color);
        void draw() override;
        void update(float frameTime);

        void initialize(
            const std::vector<std::pair<std::string, std::string>> &data,
            ArenaPreviewCallback &callback);

        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;

    private:
        void updatePosition(); ///< Update position to match current offset
        bool isShown(const TextButton &button) const;
        bool isClickable(const TextButton &button) const;

        TextButton m_upArrow, m_downArrow;
        std::vector<TextButton> m_arenaButtons;
        TextButton *m_selectedButton;

        /// Callback that will be used for preview of arenas
        ArenaPreviewCallback *m_previewCallback;
        TextureManager *m_textureManager;
        Font *m_font;
        SkinSound m_sounds;

        std::size_t m_currentLevel, m_targetLevel;
        float m_currentOffset;

        const std::size_t m_rows;
        const float m_rowHeight, m_spacing;
};

#endif
