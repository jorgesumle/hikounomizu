/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_MULTIPLE_KEY_BINDER
#define DEF_MULTIPLE_KEY_BINDER

#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/KeyBinder.hpp"

class PlayerKeys;
class TextureManager;
class SoundEngine;

/// A key binding label: Text label and the associated key binder
struct KeyLabel
{
    KeyLabel() = default;

    KeyLabel(Text &&labelText, KeyBinder &&keyBinder,
             float xBase, float xGap, float yBase,
             const Sprite &inactiveBack, const Sprite &focusedBack,
             SoundEngine &soundEngine,
             const std::string &focusSound, const std::string &keySetSound) :
    label(std::move(labelText)), binder(std::move(keyBinder))
    {
        binder.setPosition(xBase + xGap, yBase);

        binder.setInactiveBack(inactiveBack);
        binder.setFocusedBack(focusedBack);

        binder.setSoundEngine(soundEngine);
        binder.setFocusSound(focusSound);
        binder.setKeySetSound(keySetSound);

        label.setColor(Color(200, 200, 200));
        label.setPosition(xBase,
            yBase + (binder.getHeight() - label.getOriginHeight()) / 2.f);
    }

    void draw()
    {
        label.draw();
        binder.draw();
    }

    Text label; ///< A key label
    KeyBinder binder; ///< Its associated key binder
};

/// A tab interface to set the key bindings of a player
class MultipleKeyBinder : public Tab
{
    public:
        MultipleKeyBinder();
        MultipleKeyBinder(int deviceID, const PlayerKeys &initialKeys,
                          TextureManager &manager, Font &labelFont,
                          SoundEngine &soundEngine, float viewWidth);
        void draw() override;

        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;
        void keyDown(const Key &key, int deviceID);

        void setFocus(bool focused) override;

        void resetKeys(const PlayerKeys &keys);
        PlayerKeys getKeys() const;

    private:
        int m_deviceID;

        /// Key binders
        KeyLabel m_punch, m_kick, m_throwWeapon;
        KeyLabel m_jump;
        KeyLabel m_moveLeft, m_moveRight;
        KeyLabel m_crouch;
};

#endif
