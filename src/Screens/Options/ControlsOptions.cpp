/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ControlsOptions.hpp"
#include "Configuration/Configuration.hpp"
#include "Engines/Resources/FontManager.hpp"
#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Sound/SoundEngine.hpp"
#include "Structs/JoystickData.hpp"

#include "Tools/ScaledPixel.hpp"

ControlsOptions::ControlsOptions(Configuration &configuration,
                                 FontManager &fontManager,
                                 TextureManager &textureManager,
                                 SoundEngine &soundEngine,
                                 float viewWidth) :
Tab(ScaledPixel{viewWidth}(1120.f),
    ScaledPixel{viewWidth}(380.f),
    Color(66, 112, 174)),
m_keyTab_p1(configuration, 1, textureManager,
            fontManager.getDisplay(ScaledPixel{viewWidth}(20)),
            fontManager.getDefault(ScaledPixel{viewWidth}(14)),
            soundEngine, viewWidth),
m_keyTab_p2(configuration, 2, textureManager,
            fontManager.getDisplay(ScaledPixel{viewWidth}(20)),
            fontManager.getDefault(ScaledPixel{viewWidth}(14)),
            soundEngine, viewWidth)
{
    const float blankSpace = (getWidth() -
        (m_keyTab_p1.getWidth() + m_keyTab_p2.getWidth())) / 3.f;

    m_keyTab_p1.setPosition(blankSpace,
                            (getHeight() - m_keyTab_p1.getHeight()) / 2.f);
    m_keyTab_p2.setPosition(
        m_keyTab_p1.getXPosition() + m_keyTab_p1.getWidth() + blankSpace,
        (getHeight() - m_keyTab_p2.getHeight()) / 2.f);
}

void ControlsOptions::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();

    m_background.draw();

    m_keyTab_p1.draw();
    m_keyTab_p2.draw();

    Drawable::popMatrix();
}

bool ControlsOptions::mouseRelease(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);

    bool released = m_keyTab_p1.mouseRelease(localMouse);
    released = m_keyTab_p2.mouseRelease(localMouse) || released;

    return released || Widget::contains(localMouse);
}

bool ControlsOptions::mouseClick(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

bool ControlsOptions::mouseMove(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);

    bool moved = m_keyTab_p1.mouseMove(localMouse);
    moved = m_keyTab_p2.mouseMove(localMouse) || moved;

    return moved || Widget::contains(localMouse);
}

void ControlsOptions::keyEvent(const SDL_Event &event)
{
    m_keyTab_p1.keyEvent(event);
    m_keyTab_p2.keyEvent(event);
}

void ControlsOptions::addJoystickBinder(const JoystickData &joystickData)
{
    m_keyTab_p1.addJoystickBinder(joystickData);
    m_keyTab_p2.addJoystickBinder(joystickData);
}

void ControlsOptions::removeJoystickBinder(int joystickID)
{
    m_keyTab_p1.removeJoystickBinder(joystickID);
    m_keyTab_p2.removeJoystickBinder(joystickID);
}
