/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_OPTIONS_TAB
#define DEF_OPTIONS_TAB

#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/TextButton.hpp"
#include "GeneralOptions.hpp"
#include "ControlsOptions.hpp"

class Configuration;
class FontManager;
class TextureManager;
class SoundEngine;
struct JoystickData;

class OptionsTab : public Tab
{
    enum class Category { General, Controls };

    public:
        OptionsTab(Configuration &configuration, FontManager &fontManager,
                   TextureManager &textureManager, SoundEngine &soundEngine,
                   const Display::DisplayMode &activeMode,
                   float viewWidth);
        void draw() override;

        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;
        void keyEvent(const SDL_Event &event);

        void setDisplaySizes(const std::vector<std::pair<int, int>> &sizes);
        void addJoystickBinder(const JoystickData &joystickData);
        void removeJoystickBinder(int joystickID);

        /// Whether the human user wishes to apply display mode changes
        bool displayModeApplied() const;

        /// Whether the vertical sync or frame rate parameters were changed
        bool refreshRateUpdated() const;

        /// Whether the master, music or effects volume was updated
        bool volumeUpdated() const;

        /// Whether the locale was updated
        bool localeUpdated() const;

    private:
        TextButton m_generalTab, m_controlsTab;

        GeneralOptions m_general;
        ControlsOptions m_controls;

        Category m_currentCategory;
};

#endif
