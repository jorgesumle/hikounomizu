/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_LOCAL_GAME
#define DEF_LOCAL_GAME

#include "Fight/FightRules.hpp"
#include "Structs/Character.hpp"
#include "Structs/Vector.hpp"
#include "Screens/Screen.hpp"
#include <vector>
#include <string>

class GameEngine;
class Window;
class Configuration;
class FightController;

/// Screen inheriting class representing a local fight
class LocalGame : public Screen
{
    public:
        explicit LocalGame(GameEngine &gameEngine);

        void setArena(const std::string &arenaName);
        void setRules(const FightRules &rules);

        void addPlayer(const Character &player);
        void clearPlayers();

        //Screen virtual method
        void run() override;

    private:
        GameEngine *m_gameEngine;
        Window *m_window;
        Configuration *m_configuration;

        /// Parameters
        std::string m_arena;
        FightRules m_rules;
        std::vector<Character> m_playersDataList;
};

#endif
