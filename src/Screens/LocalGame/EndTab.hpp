/*Copyright (C) 2010-2023 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_END_TAB
#define DEF_END_TAB

#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/TextButton.hpp"

class TextureManager;
class SoundEngine;

/// The widget that will appear after a local game is finished.
/// It shows the winner, and allows the user to replay the game or to exit
class EndTab : public Tab
{
    public:
        /// Possible choices:
        /// ... The user did not choose an option yet
        /// ... choice to replay the game
        /// ... choice to exit
        enum class Result { NoChoice, Replay, Exit };

        EndTab(Font &textFont, TextureManager &manager,
               SoundEngine &soundEngine, float viewWidth);
        void draw() override;

        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;

        void setActive(bool active);
        bool active() const;

        bool chosen() const; ///< Has the choice been made ?
        Result result() const; ///< Result of the choice

    private:
        TextButton m_replay, m_exit;

        Result m_choice;
        bool m_active;
};

#endif
